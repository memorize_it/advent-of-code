#! /usr/bin/bash

SKELETON_PATH="./skeleton/*";

YEAR="$1";
DAY="$2";
TITLE="$3";

DAY_PATH="./${YEAR}/${DAY}_${TITLE}";

mkdir $DAY_PATH;
cp $SKELETON_PATH $DAY_PATH;
