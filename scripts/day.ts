import { ensureDir } from "https://deno.land/std/fs/mod.ts";
import { parse } from "https://deno.land/std/flags/mod.ts";

const { year = 2022, day, title } = parse(Deno.args);

if (!day || !title) {
  console.warn("Usage : deno run day.ts --day 1 --title title [--year 2022]");
  Deno.exit();
}

const dayPath = `./${year}/${day}-${title.replace(/\ /g, "_")}`;
await ensureDir(dayPath);

const skeletonPath = "./skeleton-deno";
const skeletonDir = Deno.readDirSync(skeletonPath);

for await (const dirEntry of skeletonDir) {
  if (dirEntry.isFile) {
    const fileContent = await Deno.readTextFile(
      `${skeletonPath}/${dirEntry.name}`
    );
    const dayContent = fileContent
      .replace(/\$DAY/g, day)
      .replace(/\$TITLE/g, title);

    const dayFile = `${dayPath}/${dirEntry.name}`;
    await Deno.writeTextFile(dayFile, dayContent);
  }
}

Deno.exit();
