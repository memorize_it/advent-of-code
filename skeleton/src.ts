import { parseInputAsArray } from "../utils/input";

type InputType = number[];

export async function parseInput(path: string): Promise<InputType> {
  const input = (await parseInputAsArray(path)).map((n) => Number.parseInt(n));
  return input;
}

export function part1(input: InputType) {}

export function part2(input: InputType) {}
