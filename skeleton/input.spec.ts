import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/XXX/input.txt";
const testFile = "./2020/XXX/test.input.txt";

xdescribe("X - xxxx", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 127", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(127);
      });
    });

    describe("Input file", () => {
      it("Should return 400480901", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(400480901);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 62", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(62);
      });
    });

    describe("Input file", () => {
      it("Should return 67587168", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(67587168);
      });
    });
  });
});
