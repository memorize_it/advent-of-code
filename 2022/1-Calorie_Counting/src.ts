import { parseInputAsNumber } from "../../utils/deno/input.ts";

type InputType = number[];

export const parseInput = parseInputAsNumber;

export function part1(input: InputType) {
  let maxCalories = 0;
  let currentCalories = 0;
  input.forEach((calories) => {
    if (Number.isNaN(calories)) {
      if (currentCalories > maxCalories) {
        maxCalories = currentCalories;
      }

      currentCalories = 0;
      return;
    }

    currentCalories += calories;
  });
  return maxCalories;
}

export function part2(input: InputType) {
  const { totals } = input.reduce(
    (
      calories: {
        totals: number[];
        currentCalories: number;
      },
      currentCalories
    ) => {
      if (Number.isNaN(currentCalories)) {
        calories.totals = [...calories.totals, calories.currentCalories];
        calories.currentCalories = 0;
        return calories;
      }

      calories.currentCalories += currentCalories;
      return calories;
    },
    {
      totals: [],
      currentCalories: 0,
    }
  );

  const sorted = totals.sort((a, b) => b - a);
  const [one, two, three] = sorted;
  return one + two + three;
}
