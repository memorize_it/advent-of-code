import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

export async function parseInputAsArray(path: string): Promise<string[]> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: string[] = [];
      readInterface.on("line", (line) => result.push(line));

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}
