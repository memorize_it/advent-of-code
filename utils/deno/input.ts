export async function parseInputAsString(path: string): Promise<string[]> {
  const file = await Deno.readTextFile(path);
  return file.split("\n");
}

export async function parseInputAsNumber(path: string): Promise<number[]> {
  return (await parseInputAsString(path)).map((n) => Number.parseInt(n));
}
