import { parseInputAsString } from "../../utils/deno/input.ts";

export type Board = number[][];

export type InputType = {
  drawn: number[];
  boards: Board[];
};

export const parseInput = async (path: string): Promise<InputType> => {
  const input = await parseInputAsString(path);
  const drawn = input[0].split(",").map((n) => Number.parseInt(n));
  const boards: Board[] = [];

  const boardRows = input.slice(2);
  let board: Board = [];
  for (let index = 0; index < boardRows.length; index++) {
    const element = boardRows[index];
    if (!element) {
      boards.push([...board]);
      board = [];
    } else {
      board.push(
        element
          .split(" ")
          .filter((e) => e !== "")
          .map((n) => Number.parseInt(n)),
      );
    }
  }

  return { drawn, boards };
};

const isWinnerBoard = (b: Board) => {
  for (let rowIndex = 0; rowIndex < b.length; rowIndex++) {
    const row = b[rowIndex];
    if (row.filter((e) => e >= 0).length === 0) {
      return true;
    }
  }

  for (let columnIndex = 0; columnIndex < b[0].length; columnIndex++) {
    const column = b.map((r) => r[columnIndex]);
    if (column.filter((e) => e >= 0).length === 0) {
      return true;
    }
  }
  return false;
};

const getScore = (b: Board, n: number): number => {
  return (
    n *
    b.reduce(
      (acc, row) =>
        acc + row.filter((n) => n >= 0).reduce((acc, n) => acc + n, 0),
      0,
    )
  );
};

export function part1({ drawn, boards }: InputType) {
  for (let index = 0; index < drawn.length; index++) {
    const currentNumber = drawn[index];

    for (const board in boards) {
      if (Object.prototype.hasOwnProperty.call(boards, board)) {
        const b = boards[board];
        for (let rowIndex = 0; rowIndex < b.length; rowIndex++) {
          const row = b[rowIndex];
          for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
            const e = row[columnIndex];
            if (e === currentNumber) {
              row[columnIndex] = -1;
            }
          }
        }

        if (isWinnerBoard(b)) {
          return getScore(b, currentNumber);
        }
      }
    }
  }
  return 0;
}

export function part2({ drawn, boards }: InputType) {
  const actualBoards: (Board | false)[] = boards;
  for (let index = 0; index < drawn.length; index++) {
    const currentNumber = drawn[index];

    for (const board in actualBoards) {
      if (Object.prototype.hasOwnProperty.call(actualBoards, board)) {
        const b = actualBoards[board];
        if (!b) {
          continue;
        }
        for (let rowIndex = 0; rowIndex < b.length; rowIndex++) {
          const row = b[rowIndex];
          for (let columnIndex = 0; columnIndex < row.length; columnIndex++) {
            const e = row[columnIndex];
            if (e === currentNumber) {
              row[columnIndex] = -1;
            }
          }
        }

        if (isWinnerBoard(b)) {
          if (actualBoards.filter(Boolean).length === 1) {
            return getScore(b, currentNumber);
          } else {
            actualBoards[board] = false;
          }
        }
      }
    }
  }
  return 0;
}
