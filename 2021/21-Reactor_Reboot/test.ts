import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import {
  describe,
  it,
  xdescribe,
  xit,
} from "https://deno.land/x/test_suite/mod.ts";
import { parseInput, part1, part2 } from "./src.ts";

const part1TestResult = 590784;
const part1Result = 588120;
const part2TestResult = 2758514936282235;
const part2Result = 0;

describe("21 - Reactor Reboot", () => {
  xdescribe("Part 1", () => {
    it("Test file", async () => {
      assertEquals(
        part1(await parseInput("./test.input.txt")),
        part1TestResult
      );
    });

    it("Input file", async () => {
      assertEquals(part1(await parseInput("./input.txt")), part1Result);
    });
  });

  describe("Part 2", () => {
    it("Test file", async () => {
      assertEquals(
        part2(await parseInput("./test-2.input.txt")),
        part2TestResult
      );
    });

    xit("Input file", async () => {
      assertEquals(part2(await parseInput("./input.txt")), part2Result);
    });
  });
});
