import { parseInputAsString } from "../../utils/deno/input.ts";

type Coordinate = [number, number];

interface Instruction {
  on: boolean;
  x: Coordinate;
  y: Coordinate;
  z: Coordinate;
  key: string;
}

type InputType = Instruction[];

export const parseInput = async (path: string): Promise<InputType> => {
  const input = await parseInputAsString(path);
  return input.map((s) => {
    const [order, ranges] = s.split(" ");
    const [x, y, z] = ranges.split(",").map((r) =>
      r
        .replace(/[a-z]=/, "")
        .split("..")
        .map((n) => Number.parseInt(n))
    ) as Coordinate[];
    return {
      on: order === "on",
      x,
      y,
      z,
      key: s,
    };
  });
};

export function part1(input: InputType) {
  const LIMITS: Coordinate = [-50, 50];
  return input.reduce((acc, { on, x, y, z }) => {
    const xLimits = applyBoundaries(x, LIMITS);
    const yLimits = applyBoundaries(y, LIMITS);
    const zLimits = applyBoundaries(z, LIMITS);
    if (
      xLimits?.length !== 2 ||
      yLimits?.length !== 2 ||
      zLimits?.length !== 2
    ) {
      return acc;
    }

    const [xMin, xMax] = xLimits;
    const [yMin, yMax] = yLimits;
    const [zMin, zMax] = zLimits;
    for (let currentX = xMin; currentX <= xMax; currentX++) {
      for (let currentY = yMin; currentY <= yMax; currentY++) {
        for (let currentZ = zMin; currentZ <= zMax; currentZ++) {
          const key = getCoordinateKey([currentX, currentY, currentZ]);
          if (on) {
            acc.add(key);
          } else {
            acc.delete(key);
          }
        }
      }
    }

    return acc;
  }, new Set()).size;
}

export function part2(input: InputType) {
  const freeInputs: InputType = input.filter(({ x, y, z, key }) => {
    const otherInputs = input.filter((s2) => s2.key !== key);
    const isFree = !otherInputs.find(
      ({ x: x2, y: y2, z: z2 }) =>
        includes(x, x2) && includes(y, y2) && includes(z, z2)
    );
    return isFree;
  });

  const nonFreeInputs = input.filter(
    ({ key }) => !freeInputs.find(({ key: key2 }) => key === key2)
  );

  console.log(freeInputs);
  console.log(nonFreeInputs);

  const inputOverlapping = nonFreeInputs.reduce((acc, { on, x, y, z }) => {
    const [xMin, xMax] = x;
    const [yMin, yMax] = y;
    const [zMin, zMax] = z;
    for (let currentX = xMin; currentX <= xMax; currentX++) {
      for (let currentY = yMin; currentY <= yMax; currentY++) {
        for (let currentZ = zMin; currentZ <= zMax; currentZ++) {
          const key = getCoordinateKey([currentX, currentY, currentZ]);
          if (on) {
            acc.add(key);
          } else {
            acc.delete(key);
          }
        }
      }
    }

    return acc;
  }, new Set());

  return (
    inputOverlapping.size +
    freeInputs
      .filter(({ on }) => on)
      .map(({ x, y, z }) => {
        const [xMin, xMax] = x;
        const [yMin, yMax] = y;
        const [zMin, zMax] = z;

        return Math.abs((xMax - xMin) * (yMin - yMax) * (zMin - zMax));
      })
      .reduce((acc, r) => acc + r, 0)
  );
}

const getCoordinateKey = (coordinates: number[]): string => {
  return coordinates.join("-");
};

const applyBoundaries = (
  [min, max]: Coordinate,
  [minLimit, maxLimit]: Coordinate
): Coordinate | undefined => {
  if (
    (min < minLimit && max < minLimit) ||
    (min > maxLimit && max > maxLimit)
  ) {
    return;
  }
  return [min, max];
};

const includes = (
  [aMin, aMax]: Coordinate,
  [bMin, bMax]: Coordinate
): boolean => {
  return bMin >= aMin && bMax <= aMax;
};
