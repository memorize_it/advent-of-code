import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = Array<[string, number]>;

export async function parseInput(path: string): Promise<InputType> {
  return (await parseInputAsString(path)).map((n): [string, number] => {
    const [instruction, count] = n.split(" ");
    return [instruction, Number.parseInt(count)];
  });
}

export function part1(input: InputType) {
  const position = input.reduce(
    (p, [instruction, count]) => {
      switch (instruction) {
        case "forward":
          return [p[0] + count, p[1]];
        case "down":
          return [p[0], p[1] - count];
        case "up":
          return [p[0], p[1] + count];
        default:
          return p;
      }
    },
    [0, 0],
  );

  return Math.abs(position[0] * position[1]);
}

export function part2(input: InputType) {
  const position = input.reduce(
    (p, [instruction, count]) => {
      switch (instruction) {
        case "forward":
          return [p[0] + count, p[1] - p[2] * count, p[2]];
        case "down":
          return [p[0], p[1], p[2] - count];
        case "up":
          return [p[0], p[1], p[2] + count];
        default:
          return p;
      }
    },
    [0, 0, 0],
  );

  return Math.abs(position[0] * position[1]);
}
