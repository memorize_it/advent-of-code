import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = [number, number];

export const parseInput = async (path: string): Promise<InputType> => {
  const input = await parseInputAsString(path);
  return input.map((s) => Number.parseInt(s[s.length - 1])) as InputType;
};

interface Game {
  // Each player has a position and a score
  // [position, score]
  // [position is 0..9]
  players: [number, number][];
  // Last value on previous turn (starts with 0)
  dice: number;
  diceRolls: number;
  loserScore?: number;
}

export function part1(input: InputType) {
  const players: [number, number][] = input.map((n) => [n - 1, 0]);
  let game: Game = { players, dice: 0, diceRolls: 0 };

  while (!game.loserScore) {
    game = play(game);
  }

  return game.diceRolls * game.loserScore;
}

const play = ({ players, dice, diceRolls }: Game): Game => {
  let d = dice;

  const newPlayers: [number, number][] = [];

  for (let index = 0; index < players.length; index++) {
    const [position, score] = players[index];
    let moves = 0;

    d = rollDice(d);
    diceRolls++;
    moves = moves + d;

    d = rollDice(d);
    diceRolls++;
    moves = moves + d;

    d = rollDice(d);
    diceRolls++;
    moves = moves + d;

    const newPosition = (position + moves) % 10;
    const newScore = score + newPosition + 1;
    newPlayers.push([newPosition, newScore]);

    if (newScore === 1000) {
      return {
        players: newPlayers,
        dice: d,
        diceRolls,
        loserScore: newPlayers.length === 2 ? newPlayers[0][1] : players[1][1],
      };
    }
  }

  return { players: newPlayers, dice: d, diceRolls };
};

function rollDice(d: number) {
  d++;
  return d > 100 ? 1 : d;
}

export function part2(input: InputType) {
  const possibilitiesInEveryRoll = getPossibleRollsAndOccurences();
  const newPositionMap = possibleNewPositionsByStartPosition(
    possibilitiesInEveryRoll,
  );

  let p1Wins = 0;
  let p2Wins = 0;

  const [p1, p2] = input;
  let universes: Map<string, number> = new Map();
  universes.set(getKey(p1 - 1, 0, p2 - 1, 0), 1); // 1 because we are shifting the array to start with 0

  while (universes.size) {
    const newUniverses: Map<string, number> = new Map();
    universes.forEach((occurrences, key) => {
      const [[p1, s1], [p2, s2]] = keyToGame(key);
      const remainingP1Possibilities: Map<[number, number], number> = new Map();
      newPositionMap.get(p1)?.forEach(([newP1, newOccurrences]) => {
        const newS1 = s1 + newP1 + 1;
        const totalOccurrences = occurrences * newOccurrences;
        if (newS1 >= 21) {
          p1Wins = p1Wins + totalOccurrences;
          return;
        }

        remainingP1Possibilities.set([newP1, newS1], totalOccurrences);
      });

      const p2Possibilities = newPositionMap.get(p2);

      remainingP1Possibilities.forEach((p1Occurrences, [newP1, newS1]) => {
        p2Possibilities?.forEach(([newP2, newOccurrences]) => {
          const newS2 = s2 + newP2 + 1;
          const totalOccurrences = p1Occurrences * newOccurrences;
          if (newS2 >= 21) {
            p2Wins = p2Wins + totalOccurrences;
            return;
          }

          const key = getKey(newP1, newS1, newP2, newS2);
          const existing = newUniverses.get(key);
          newUniverses.set(
            key,
            existing ? existing + totalOccurrences : totalOccurrences,
          );
        });
      });
    });

    universes = newUniverses;
  }

  return p1Wins > p2Wins ? p1Wins : p2Wins;
}

const getPossibleRollsAndOccurences = () => {
  const possibilitites: Map<number, number> = new Map();

  for (let first = 1; first <= 3; first++) {
    for (let second = 1; second <= 3; second++) {
      for (let third = 1; third <= 3; third++) {
        const possibility = first + second + third;
        const existing = possibilitites.get(possibility);
        possibilitites.set(possibility, existing ? existing + 1 : 1);
      }
    }
  }

  return possibilitites;
};

const BOARD = [0, 1, 2, 3, 4, 5, 6, 7, 8, 9];

const possibleNewPositionsByStartPosition = (
  possibleRollsAndOccurences: Map<number, number>,
) => {
  const possibilitiesArray = Array.of(...possibleRollsAndOccurences.entries());
  // New Possition, Amount
  return BOARD.reduce((map: Map<number, [number, number][]>, startPosition) => {
    map.set(
      startPosition,
      possibilitiesArray.map(([score, occurrences]) => [
        (startPosition + score) % 10,
        occurrences,
      ]),
    );
    return map;
  }, new Map());
};

const getKey = (p1: number, s1: number, p2: number, s2: number): string => {
  return `${p1} - ${s1} | ${p2} - ${s2}`;
};

const keyToGame = (key: string): [number, number][] => {
  return key
    .split(" | ")
    .map(
      (player) =>
        player.split(" - ").map((n) => Number.parseInt(n)) as [number, number],
    );
};
