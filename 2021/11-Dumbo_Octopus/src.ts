import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = number[][];

export const parseInput = async (path: string) => {
  const parsed = await parseInputAsString(path);
  return parsed.map((n) => n.split("").map((n) => Number.parseInt(n)));
};

export function part1(input: InputType) {
  let result = turn({ input, flashes: 0 });
  for (let index = 0; index < 99; index++) {
    result = turn(result);
  }

  return result.flashes;
}

function turn({ input, flashes }: { input: InputType; flashes: number }): {
  input: InputType;
  flashes: number;
} {
  const newInput: InputType = input.reduce((acc: number[][], n: number[]) => {
    acc.push(n.map((n) => n + 1));
    return acc;
  }, []);

  let flashedOctopuses: Array<[number, number]> = [];
  let newFlashes = getFlashingOctopuses(newInput, flashedOctopuses);

  while (newFlashes.length) {
    newFlashes.forEach(([line, column]) => {
      update(newInput, line - 1, column);
      update(newInput, line + 1, column);
      update(newInput, line, column, false);
    });
    flashedOctopuses = [...flashedOctopuses, ...newFlashes];
    newFlashes = getFlashingOctopuses(newInput, flashedOctopuses);
  }

  flashedOctopuses.forEach(([column, line]) => {
    newInput[column][line] = 0;
  });

  return {
    input: newInput,
    flashes: flashes + flashedOctopuses.length,
  };
}

const getFlashingOctopuses = (
  input: InputType,
  alreadyFlashed: Array<[number, number]>,
) => {
  return input.reduce((acc: Array<[number, number]>, line, lineNumber) => {
    line.forEach((v, columnNumber) => {
      const currentPosition: [number, number] = [lineNumber, columnNumber];
      if (alreadyFlashed.find((n) => n.join("") === currentPosition.join(""))) {
        return;
      }

      if (v > 9) {
        acc.push(currentPosition);
      }
    });

    return acc;
  }, []);
};

function update(
  newInput: number[][],
  lineNumber: number,
  columnNumber: number,
  updateSelf = true,
) {
  const line = newInput[lineNumber];
  if (!line) {
    return;
  }

  const previousColumn = columnNumber - 1;
  if (previousColumn >= 0) {
    line[previousColumn] = line[previousColumn] + 1;
  }
  if (updateSelf) {
    line[columnNumber] = line[columnNumber] + 1;
  }
  const nextColumn = columnNumber + 1;
  if (nextColumn < line.length) {
    line[nextColumn] = line[nextColumn] + 1;
  }
}

export function part2(input: InputType) {
  let result = turn({ input, flashes: 0 });
  let previousFlashes = 0;
  let currentTurn = 1;

  while (result.flashes - previousFlashes !== 100) {
    currentTurn++;
    previousFlashes = result.flashes;
    result = turn(result);
  }

  return currentTurn;
}
