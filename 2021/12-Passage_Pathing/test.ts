import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { describe, it } from "https://deno.land/x/test_suite/mod.ts";
import { parseInput, part1, part2 } from "./src.ts";

const part1TestEasyResult = 10;
const part1TestMediumResult = 19;
const part1TestHardResult = 226;
const part1Result = 3510;
const part2TestEasyResult = 36;
const part2TestMediumResult = 103;
const part2TestHardResult = 3509;
const part2Result = 0;

describe("12 - Passage Pathing", () => {
  describe("Part 1", () => {
    it("Test file - Easy", async () => {
      assertEquals(
        part1(await parseInput("./test.input.easy.txt")),
        part1TestEasyResult,
      );
    });

    it("Test file - Medium", async () => {
      assertEquals(
        part1(await parseInput("./test.input.medium.txt")),
        part1TestMediumResult,
      );
    });

    it("Test file - Hard", async () => {
      assertEquals(
        part1(await parseInput("./test.input.hard.txt")),
        part1TestHardResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part1(await parseInput("./input.txt")), part1Result);
    });
  });

  describe("Part 2", () => {
    it("Test file - Easy", async () => {
      assertEquals(
        part2(await parseInput("./test.input.easy.txt")),
        part2TestEasyResult,
      );
    });

    it("Test file - Medium", async () => {
      assertEquals(
        part2(await parseInput("./test.input.medium.txt")),
        part2TestMediumResult,
      );
    });

    it("Test file - Hard", async () => {
      assertEquals(
        part2(await parseInput("./test.input.hard.txt")),
        part2TestHardResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part2(await parseInput("./input.txt")), part2Result);
    });
  });
});
