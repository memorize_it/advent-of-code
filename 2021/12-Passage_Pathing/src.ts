import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = Map<string, string[]>;

export const parseInput = async (path: string): Promise<InputType> => {
  const input = await parseInputAsString(path);
  return input
    .map((n) => n.split("-"))
    .reduce((map: Map<string, string[]>, [enter, exit]) => {
      const one = map.get(enter);
      if (one) {
        one.push(exit);
      } else {
        map.set(enter, [exit]);
      }

      const two = map.get(exit);
      if (two) {
        two.push(enter);
      } else {
        map.set(exit, [enter]);
      }

      return map;
    }, new Map());
};

export function part1(input: InputType) {
  let remove: string[] = getCavesToRemove(input);
  while (remove.length) {
    remove.forEach((caveThatMustBeIgnored) => {
      input.delete(caveThatMustBeIgnored);
      input.forEach((possibleCaves, currentCave) => {
        input.set(
          currentCave,
          possibleCaves.filter((cave) => cave !== caveThatMustBeIgnored),
        );
      });
    });
    remove = getCavesToRemove(input);
  }

  return getPaths(input, "start", []);
}

function getCavesToRemove(input: InputType) {
  const remove: string[] = [];

  input.forEach((possibleCaves, currentCave) => {
    if (!isBig(currentCave)) {
      if (
        !possibleCaves.length ||
        (possibleCaves.length === 1 && !possibleCaves.find(isBig))
      ) {
        remove.push(currentCave); //Current cave is only accessible by a single small cave
      }
    }
  });

  return remove;
}

export function part2(input: InputType) {
  return getPaths2(input, "start", []);
}

function isBig(s: string) {
  return s.toUpperCase() === s;
}

function getPaths(map: InputType, start: string, visited: string[]): number {
  if (start === "end") {
    return 1;
  }

  let pathsToGo = map.get(start);
  if (!pathsToGo || !pathsToGo.length) {
    return 0;
  }

  if (visited.length) {
    pathsToGo = pathsToGo.filter((n) => isBig(n) || !visited.includes(n));
  }

  return pathsToGo
    .map((n) => getPaths(map, n, [...visited, start]))
    .reduce((acc, values) => acc + values, 0);
}

function getPaths2(
  map: InputType,
  start: string,
  visited: string[],
  alreadyVisitedTwice = false,
): number {
  if (start === "end") {
    return 1;
  }

  let pathsToGo = map.get(start);
  if (!pathsToGo || !pathsToGo.length) {
    return 0;
  }

  if (visited.length) {
    pathsToGo = pathsToGo.filter((n) => {
      if (isBig(n)) {
        return true;
      }

      if (n === "start") {
        return false;
      }

      if (alreadyVisitedTwice) {
        return !visited.includes(n);
      } else {
        return true;
      }
    });
  }

  return pathsToGo
    .map((n) =>
      getPaths2(
        map,
        n,
        [...visited, start],
        alreadyVisitedTwice ? true : !isBig(n) && visited.includes(n),
      )
    )
    .reduce((acc, values) => acc + values, 0);
}
