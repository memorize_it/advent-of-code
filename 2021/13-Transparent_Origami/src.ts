import { parseInputAsString } from "../../utils/deno/input.ts";

type Coordinate = [number, number];

type InputType = {
  coordinates: Coordinate[];
  folds: Coordinate[];
};

// x === column
// y === line
// [x, y]

export const parseInput = async (path: string) => {
  const input = await parseInputAsString(path);
  const coordinates: Coordinate[] = [];
  const folds: Coordinate[] = [];
  let parsingFoldings = false;

  input.forEach((s) => {
    if (s === "") {
      parsingFoldings = true;
      return;
    }

    if (parsingFoldings) {
      const [, folding] = s.split("fold along ");
      const [direction, amount] = folding.split("=");
      if (direction === "x") {
        folds.push([Number.parseInt(amount), 0]);
      } else {
        folds.push([0, Number.parseInt(amount)]);
      }

      return;
    }

    const [x, y] = s.split(",").map((n) => Number.parseInt(n));
    coordinates.push([x, y]);
  });

  return { coordinates, folds };
};

export function part1({ coordinates, folds }: InputType) {
  const result = [folds[0]].reduce((previousState: Set<Coordinate>, fold) => {
    const [x, y] = fold;
    const newState = new Set<Coordinate>();
    if (x) {
      //foldOnColumn
      previousState.forEach(([xP, yP]) => {
        if (xP < x) {
          newState.add([xP, yP]);
        } else if (xP > x) {
          newState.add([2 * x - xP, yP]);
        }
      });
    } else {
      //foldOnRow
      previousState.forEach(([xP, yP]) => {
        if (yP < y) {
          newState.add([xP, yP]);
        } else if (yP > y) {
          newState.add([xP, 2 * y - yP]);
        }
      });
    }
    return newState;
  }, new Set(coordinates));

  const r = Array.of(...result)
    .map(([x, y]) => `${x}-${y}`)
    .sort();
  const uniqueResult = r.filter((n, i) => r.indexOf(n) === i);
  return uniqueResult.length;
}

export function part2({ coordinates, folds }: InputType) {
  const result = folds.reduce((previousState: Set<Coordinate>, fold) => {
    const [x, y] = fold;
    const newState = new Set<Coordinate>();
    if (x) {
      //foldOnColumn
      previousState.forEach(([xP, yP]) => {
        if (xP < x) {
          newState.add([xP, yP]);
        } else if (xP > x) {
          newState.add([2 * x - xP, yP]);
        }
      });
    } else {
      //foldOnRow
      previousState.forEach(([xP, yP]) => {
        if (yP < y) {
          newState.add([xP, yP]);
        } else if (yP > y) {
          newState.add([xP, 2 * y - yP]);
        }
      });
    }
    return newState;
  }, new Set(coordinates));

  const r = Array.of(...result)
    .map(([x, y]) => `${x}-${y}`)
    .sort();
  const uniqueResult = r.filter((n, i) => r.indexOf(n) === i);

  const { x, y } = Array.of(...result).reduce(
    (acc, [x, y]) => {
      if (x > acc.x) {
        acc.x = x;
      }
      if (y > acc.y) {
        acc.y = y;
      }
      return acc;
    },
    { x: 0, y: 0 },
  );

  const matrix: string[][] = [];
  for (let line = 0; line <= y; line++) {
    const l = [];
    for (let column = 0; column <= x; column++) {
      l.push(".");
    }
    matrix.push(l);
  }

  Array.of(...result).forEach(([x, y]) => {
    matrix[y][x] = "X";
  });

  console.log("\n" + matrix.map((l) => l.join("")).join("\n"));

  return uniqueResult.length;
}
