import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = {
  start: string;
  rules: Map<string, string>;
};

export const parseInput = async (path: string) => {
  const input = await parseInputAsString(path);
  const start = input[0];
  return {
    start,
    rules: input.slice(2).reduce((map, string) => {
      const [key, value] = string.split(" -> ");
      map.set(key, value);
      return map;
    }, new Map()),
  };
};

export function part1(input: InputType) {
  return executeTurns(input, 10);
}

export function part2(input: InputType) {
  return executeTurns(input, 40);
}

function executeTurns({ start, rules }: InputType, iterations: number) {
  let currentState = new Map();
  const caracters = start.split("");

  for (let index = 0; index < caracters.length - 1; index++) {
    const element = caracters[index] + caracters[index + 1];
    currentState.set(element, (currentState.get(element) || 0) + 1);
  }

  for (let step = 0; step < iterations; step++) {
    currentState = turn(currentState, rules);
  }

  const countPerValue = getCounts(currentState);
  countPerValue.set(start[0], (countPerValue.get(start[0]) || 0) + 1);
  countPerValue.set(
    start[start.length - 1],
    (countPerValue.get(start[start.length - 1]) || 0) + 1,
  );
  const values: number[] = Array.of(...countPerValue.values()).map(
    (a) => a / 2,
  );

  values.sort((a, b) => a - b);

  const lessCommonCount = values[0];
  const mostCommonCount = values[values.length - 1];
  return mostCommonCount - lessCommonCount;
}

function turn(
  currentCount: Map<string, number>,
  rules: Map<string, string>,
): Map<string, number> {
  const newCount: Map<string, number> = new Map();
  for (const [pair, count] of currentCount.entries()) {
    const [first, second] = pair.split("");
    const newCharacter = rules.get(pair);
    if (!newCharacter) {
      throw new Error();
    }

    newCount.set(
      first + newCharacter,
      (newCount.get(first + newCharacter) || 0) + count,
    );

    newCount.set(
      newCharacter + second,
      (newCount.get(newCharacter + second) || 0) + count,
    );
  }
  return newCount;
}

const getCounts = (currentState: Map<string, number>): Map<string, number> => {
  const newCount: Map<string, number> = new Map();

  for (const [pair, count] of currentState.entries()) {
    const [first, second] = pair.split("");
    newCount.set(first, (newCount.get(first) || 0) + count);
    newCount.set(second, (newCount.get(second) || 0) + count);
  }

  return newCount;
};
