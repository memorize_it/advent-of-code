import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = number[];

export const parseInput = async (path: string) => {
  return (await parseInputAsString(path))[0]
    .split(",")
    .map((n) => Number.parseInt(n));
};

export function part1(input: InputType) {
  return fuelConsumption(input, (n) => n);
}

export function fuelConsumption(
  input: InputType,
  consume: (unitsToMove: number) => number,
) {
  const min = Math.min(...input);
  const max = Math.max(...input);

  const options = input.reduce((options, initialPosition) => {
    const exisitng = options.get(initialPosition);
    if (exisitng !== undefined) {
      options.set(initialPosition, exisitng + 1);
    } else {
      options.set(initialPosition, 1);
    }
    return options;
  }, new Map<number, number>());

  const distances = new Map<number, Map<number, number>>();

  // Multiply by the number of occurrences

  options.forEach((occurences, initialPostiton) => {
    const positions = new Map<number, number>();

    for (let targetPosition = min; targetPosition <= max; targetPosition++) {
      positions.set(
        targetPosition,
        occurences * consume(Math.abs(initialPostiton - targetPosition)),
      );
    }

    distances.set(initialPostiton, positions);
  });

  let minFuel = Number.MAX_VALUE;

  for (let targetPosition = min; targetPosition <= max; targetPosition++) {
    let fuel = 0;

    distances.forEach((positions) => {
      fuel = fuel + (positions.get(targetPosition) || 0);
    });

    if (minFuel > fuel) {
      minFuel = fuel;
    }
  }

  return minFuel;
}

export function part2(input: InputType) {
  return fuelConsumption(input, (n) => (n * (n + 1)) / 2);
}
