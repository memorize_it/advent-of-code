import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = string[][];

export const parseInput = async (path: string) => {
  return (await parseInputAsString(path)).map((s) => s.split(""));
};

const closing: { [key: string]: string } = {
  "(": ")",
  "[": "]",
  "{": "}",
  "<": ">",
};

const points: { [key: string]: number } = {
  ")": 3,
  "]": 57,
  "}": 1197,
  ">": 25137,
};

export function part1(input: InputType) {
  return input
    .map((line): number => {
      const attendedCharacters: string[] = [];
      for (let index = 0; index < line.length; index++) {
        const char = line[index];
        const closingCharacter = closing[char];
        const isOpening = closingCharacter !== undefined;
        if (isOpening) {
          attendedCharacters.unshift(closingCharacter);
          continue;
        }

        // Current character is a closing
        if (!attendedCharacters.length) {
          // Not expecting any closing
          return points[char];
        }

        if (char !== attendedCharacters[0]) {
          // Not the expected closing
          return points[char];
        }

        attendedCharacters.shift();
      }
      return 0;
    })
    .filter(Boolean)
    .reduce((acc: number, n: number) => acc + n, 0);
}
const points2: { [key: string]: number } = {
  ")": 1,
  "]": 2,
  "}": 3,
  ">": 4,
};

export function part2(input: InputType) {
  const result = input
    .map((line) => {
      const attendedCharacters: string[] = [];
      for (let index = 0; index < line.length; index++) {
        const char = line[index];
        const closingCharacter = closing[char];
        const isOpening = closingCharacter !== undefined;
        if (isOpening) {
          attendedCharacters.unshift(closingCharacter);
          continue;
        }

        // Current character is a closing
        if (!attendedCharacters.length) {
          // Not expecting any closing
          return;
        }

        if (char !== attendedCharacters[0]) {
          // Not the expected closing
          return;
        }

        attendedCharacters.shift();
      }
      return attendedCharacters;
    })
    .filter(Boolean)
    .map((attended) =>
      (attended as string[])
        .map((a) => points2[a])
        .reduce((acc, p) => acc * 5 + p, 0)
    )
    .sort((a, b) => a - b);
  return result[Math.floor(result.length / 2)];
}
