import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = number[][];

export const parseInput = async (path: string) => {
  const input = await parseInputAsString(path);
  return input.map((s) => s.split("").map((n) => Number.parseInt(n)));
};

interface Node {
  edgeValue: number;
  tentativeDistance: number;
  neighbours: string[];
}

export function part1(input: InputType) {
  const nodeMap: Map<string, Node> = new Map();
  const unvisitedNodes: Set<string> = new Set();

  const maxLines = input.length - 1;
  const maxColumns = input[0].length - 1;

  input.forEach((line, lineNumber) =>
    line.forEach((edgeValue, columnNumber) => {
      const key = getKey(lineNumber, columnNumber);
      unvisitedNodes.add(key);
      nodeMap.set(
        key,
        createNode(lineNumber, columnNumber, maxLines, maxColumns, edgeValue),
      );
    })
  );

  const startNode = getKey(0, 0);
  const endNode = getKey(maxLines, maxColumns);
  return getShortestPath(nodeMap, unvisitedNodes, startNode, endNode);
}

export function part2(input: InputType) {
  const nodeMap: Map<string, Node> = new Map();
  const unvisitedNodes: Set<string> = new Set();

  const lines = input.length;
  const columns = input[0].length;
  const maxLines = lines * 5 - 1;
  const maxColumns = columns * 5 - 1;

  for (let lineMultiplier = 0; lineMultiplier < 5; lineMultiplier++) {
    for (let columnMultiplier = 0; columnMultiplier < 5; columnMultiplier++) {
      input.forEach((line, lineNumber) => {
        const currentLine = lineNumber + lineMultiplier * lines;
        line.forEach((edgeValue, columnNumber) => {
          const currentColumn = columnNumber + columnMultiplier * columns;
          const key = getKey(currentLine, currentColumn);
          unvisitedNodes.add(key);
          nodeMap.set(
            key,
            createNode(
              currentLine,
              currentColumn,
              maxLines,
              maxColumns,
              getValue(edgeValue, lineMultiplier, columnMultiplier),
            ),
          );
        });
      });
    }
  }

  const startNode = getKey(0, 0);
  const endNode = getKey(maxLines, maxColumns);
  return getShortestPath(nodeMap, unvisitedNodes, startNode, endNode);
}

export function getShortestPath(
  nodeMap: Map<string, Node>,
  unvisitedNodes: Set<string>,
  startNode: string,
  endNode: string,
): number {
  const weightedNodes: Set<string> = new Set();
  let currentNode = startNode;
  while (unvisitedNodes.has(endNode)) {
    const node = nodeMap.get(currentNode);
    if (!node) {
      throw new Error();
    }

    node.neighbours
      .filter((n) => unvisitedNodes.has(n))
      .forEach((n) => {
        const neighbour = nodeMap.get(n);
        if (!neighbour) {
          throw new Error();
        }

        const newTentative = node.tentativeDistance + neighbour.edgeValue;
        if (newTentative < neighbour.tentativeDistance) {
          nodeMap.set(n, { ...neighbour, tentativeDistance: newTentative });
          weightedNodes.add(n);
        }
      });
    weightedNodes.delete(currentNode);
    unvisitedNodes.delete(currentNode);

    let minNode: string | undefined;
    let minDistance = Number.MAX_SAFE_INTEGER;

    if (weightedNodes.size) {
      weightedNodes.forEach((key) => {
        const tentativeDistance = nodeMap.get(key)?.tentativeDistance;
        if (!tentativeDistance) {
          throw new Error();
        }

        if (tentativeDistance < minDistance) {
          minNode = key;
          minDistance = tentativeDistance;
        }
      });

      if (!minNode) {
        throw new Error();
      }

      currentNode = minNode;
    }
  }

  return nodeMap.get(endNode)?.tentativeDistance || 0;
}

const createNode = (
  lineNumber: number,
  columnNumber: number,
  maxLines: number,
  maxColumns: number,
  edgeValue: number,
): Node => {
  const neighbours: string[] = [];
  if (lineNumber - 1 > -1) {
    neighbours.push(getKey(lineNumber - 1, columnNumber));
  }

  if (lineNumber + 1 <= maxLines) {
    neighbours.push(getKey(lineNumber + 1, columnNumber));
  }

  if (columnNumber - 1 > -1) {
    neighbours.push(getKey(lineNumber, columnNumber - 1));
  }

  if (columnNumber + 1 <= maxColumns) {
    neighbours.push(getKey(lineNumber, columnNumber + 1));
  }
  return {
    edgeValue,
    tentativeDistance: lineNumber === 0 && columnNumber === 0
      ? 0
      : Number.MAX_SAFE_INTEGER,
    neighbours,
  };
};

const getKey = (x: number, y: number) => {
  return `${x} - ${y}`;
};

const getValue = (
  startValue: number,
  lineMultiplier: number,
  columnMultiplier: number,
): number => {
  const n = startValue + lineMultiplier + columnMultiplier;
  return n - 9 * Math.floor(n / 9) || 9;
};
