import { parseInputAsNumber } from "../../utils/deno/input.ts";

type InputType = number[];

export const parseInput = parseInputAsNumber;

export function part1(input: InputType) {
  let counter = 0;
  input.reduce((previous, current) => {
    if (current > previous) {
      counter++;
    }
    return current;
  }, Number.MAX_VALUE);

  return counter;
}

export function part2(input: InputType) {
  const condensed = [];

  for (let index = 2; index < input.length; index++) {
    condensed.push(input[index - 2] + input[index - 1] + input[index]);
  }

  return part1(condensed);
}
