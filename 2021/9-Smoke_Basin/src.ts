import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = number[][];

export const parseInput = async (path: string): Promise<InputType> => {
  const lines = await parseInputAsString(path);
  return lines.map((l) => l.split("").map((n) => Number.parseInt(n)));
};

export function part1(input: InputType) {
  return input.reduce((acc, line, lineNumber) => {
    const previousLine = input[lineNumber - 1];
    const nextLine = input[lineNumber + 1];
    const lineCount: number = line.reduce((lineAcc, point, pointNumber) => {
      const up = previousLine ? previousLine[pointNumber] : Number.MAX_VALUE;
      const bottom = nextLine ? nextLine[pointNumber] : Number.MAX_VALUE;
      const left = line[pointNumber - 1] !== undefined
        ? line[pointNumber - 1]
        : Number.MAX_VALUE;
      const right = line[pointNumber + 1] !== undefined
        ? line[pointNumber + 1]
        : Number.MAX_VALUE;

      if (point < up && point < bottom && point < left && point < right) {
        return lineAcc + point + 1;
      }
      return lineAcc;
    }, 0);
    return acc + lineCount;
  }, 0);
}

export function part2(input: InputType) {
  const lowPoints = getLowPoints(input);
  const map = parseNeighbours(input);
  const result = lowPoints
    .map(([line, column]) => {
      const accumulator: string[] = [];
      countNeighbours(getId(line, column), map, accumulator);
      return accumulator.length;
    })
    .sort((a, b) => b - a);

  return result[0] * result[1] * result[2];
}

function countNeighbours(
  id: string,
  neighbourMap: Map<string, string[]>,
  accumulator: string[],
) {
  if (accumulator.find((n) => n === id)) {
    return;
  }
  accumulator.push(id);

  const neighbours = neighbourMap.get(id);
  if (neighbours) {
    neighbours
      .filter((n) => !accumulator.includes(n))
      .forEach((n) => countNeighbours(n, neighbourMap, accumulator));
  }
}

function getLowPoints(input: InputType): Array<[number, number]> {
  return input.reduce((acc: Array<[number, number]>, line, lineNumber) => {
    const previousLine = input[lineNumber - 1];
    const nextLine = input[lineNumber + 1];
    const lineLowerPoints: Array<[number, number]> = line.reduce(
      (lineAcc: Array<[number, number]>, point, columnNumber) => {
        const up = previousLine ? previousLine[columnNumber] : Number.MAX_VALUE;
        const bottom = nextLine ? nextLine[columnNumber] : Number.MAX_VALUE;
        const left = line[columnNumber - 1] !== undefined
          ? line[columnNumber - 1]
          : Number.MAX_VALUE;
        const right = line[columnNumber + 1] !== undefined
          ? line[columnNumber + 1]
          : Number.MAX_VALUE;

        if (point < up && point < bottom && point < left && point < right) {
          return [...lineAcc, [lineNumber, columnNumber]];
        }
        return lineAcc;
      },
      [],
    );
    return [...acc, ...lineLowerPoints];
  }, []);
}

function parseNeighbours(input: InputType): Map<string, string[]> {
  const acc: Map<string, string[]> = new Map();
  input.forEach((line, lineNumber) => {
    line.forEach((point, columnNumber) => {
      if (point === 9) {
        return;
      }

      const neighbours: string[] = [
        [lineNumber - 1, columnNumber],
        [lineNumber, columnNumber - 1],
        [lineNumber, columnNumber + 1],
        [lineNumber + 1, columnNumber],
      ]
        .map(([l, c]) => {
          const p = (input[l] || [])[c];
          if (p !== undefined && p !== 9) {
            return getId(l, c);
          }
          return "";
        })
        .filter(Boolean);
      acc.set(getId(lineNumber, columnNumber), neighbours);
    }, []);
  });

  return acc;
}

function getId(x: number, y: number) {
  return `${x}-${y}`;
}
