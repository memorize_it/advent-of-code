import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = {
  signalPattern: string[];
  digitOutput: string[];
}[];

export const parseInput = async (path: string): Promise<InputType> => {
  const lines = await parseInputAsString(path);
  return lines.map((l) => {
    const [signal, digit] = l.split(" | ");
    return {
      signalPattern: signal.split(" "),
      digitOutput: digit.split(" "),
    };
  });
};

export function part1(input: InputType) {
  return input
    .map(({ digitOutput }) => digitOutput)
    .reduce((acc, digits) => {
      return acc + digits.filter((d) => [2, 4, 3, 7].includes(d.length)).length;
    }, 0);
}

type Project = {
  base: string;
  remainingItems: string[];
  final: string[];
};

function process(
  { base, remainingItems, final }: Project,
  search: string[],
  replacement: string[],
): Project {
  const allPresent = search.every((v) => remainingItems.includes(v));
  const newRemainingItems = allPresent
    ? remainingItems.filter((n) => !search.includes(n))
    : remainingItems;
  const newFinal = allPresent ? final.concat(...replacement) : final;

  return { base, remainingItems: newRemainingItems, final: newFinal };
}

export const parseInput2 = async (path: string): Promise<InputType2> => {
  const lines = await parseInputAsString(path);
  return lines.map((l) => {
    const [signal, digit] = l.split(" | ");
    return {
      signalPattern: signal.split(" ").map((base) => ({
        base: base.split("").sort().join(""),
        remainingItems: base.split("").sort(),
        final: [],
      })),
      digitOutput: digit.split(" ").map((n) => n.split("").sort().join("")),
    };
  });
};

type InputType2 = InputChunk[];

type InputChunk = {
  signalPattern: Project[];
  digitOutput: string[];
};

export function part2(input: InputType2) {
  return input.map(decodePattern).reduce((acc, n) => acc + n, 0);
}

const digits = new Map();
digits.set("abcefg", 0);
digits.set("cf", 1);
digits.set("acdeg", 2);
digits.set("acdfg", 3);
digits.set("bcdf", 4);
digits.set("abdfg", 5);
digits.set("abdefg", 6);
digits.set("acf", 7);
digits.set("abcdefg", 8);
digits.set("abcdfg", 9);

const missingNumbers = [
  "abcefg",
  "acdeg",
  "acdfg",
  "abdfg",
  "abdefg",
  "abcdfg",
].map((s) => s.split(""));

function decodePattern({ digitOutput, signalPattern }: InputChunk) {
  // Find and replace one
  const one =
    signalPattern.find(({ base }) => base.length === 2)?.remainingItems || [];
  let nextStep = apply(signalPattern, one, "cf");

  // Find and replace remaining seven
  const seven =
    nextStep.find(({ base }) => base.length === 3)?.remainingItems || [];
  const sevenWithoutOne = seven.filter((n) => !one.includes(n));
  nextStep = apply(nextStep, sevenWithoutOne, "a");

  // Find and replace remaining four
  const four = nextStep.find(({ base }) => base.length === 4)?.remainingItems ||
    [];
  const fourPair = four.filter((n) => !one.includes(n));
  nextStep = apply(nextStep, fourPair, "bd");

  // Remaining digits will be isolated in several iterations
  while (nextStep.find(({ remainingItems }) => remainingItems.length > 0)) {
    for (const item in nextStep) {
      const { remainingItems, final } = nextStep[item];
      if (remainingItems.length === 1) {
        const targetLength = final.length + 1;
        const missing = missingNumbers.find(
          (n) => n.length === targetLength && final.every((e) => n.includes(e)),
        ) || [];

        if (missing.length) {
          nextStep = applyOnArray(
            nextStep,
            remainingItems,
            missing.filter((n) => !final.includes(n)),
          );
        }
      }
    }
  }

  // Clean results
  const decodeFinished = nextStep.reduce((map, { base, final }) => {
    map.set(base, final.sort().join(""));
    return map;
  }, new Map());

  const digitsDecoded = digitOutput.map((d) => {
    const original = decodeFinished.get(d);
    return digits.get(original);
  });

  return Number.parseInt(digitsDecoded.reduce((acc, p) => acc + p, ""));
}

function apply(
  signalPattern: Project[],
  search: string[],
  replacement: string,
) {
  return applyOnArray(signalPattern, search, replacement.split(""));
}

function applyOnArray(
  signalPattern: Project[],
  search: string[],
  replacement: string[],
) {
  return signalPattern.map((p) => process(p, search, replacement));
}
