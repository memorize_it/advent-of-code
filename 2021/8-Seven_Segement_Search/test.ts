import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import {
  describe,
  it,
  xdescribe,
  xit,
} from "https://deno.land/x/test_suite/mod.ts";
import { parseInput, parseInput2, part1, part2 } from "./src.ts";

const part1TestResult = 26;
const part1Result = 392;
const part2Test0Result = 5353;
const part2TestResult = 61229;
const part2Result = 1004688;

describe("8 - Seven Segement Search", () => {
  describe("Part 1", () => {
    it("Test file", async () => {
      assertEquals(
        part1(await parseInput("./test.input.txt")),
        part1TestResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part1(await parseInput("./input.txt")), part1Result);
    });
  });

  describe("Part 2", () => {
    it("Test 0 file", async () => {
      assertEquals(
        part2(await parseInput2("./test-0.input.txt")),
        part2Test0Result,
      );
    });

    it("Test file", async () => {
      assertEquals(
        part2(await parseInput2("./test.input.txt")),
        part2TestResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part2(await parseInput2("./input.txt")), part2Result);
    });
  });
});
