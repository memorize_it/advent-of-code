import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { describe, it } from "https://deno.land/x/test_suite/mod.ts";
import { parseInput, part1, part2 } from "./src.ts";

const part1TestResult = 198;
const part1Result = 749376;
const part2TestResult = 230;
const part2Result = 2372923;

describe("3 - Binary Diagnostic", () => {
  describe("Part 1", () => {
    it("Test file", async () => {
      assertEquals(
        part1(await parseInput("./test.input.txt")),
        part1TestResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part1(await parseInput("./input.txt")), part1Result);
    });
  });

  describe("Part 2", () => {
    it("Test file", async () => {
      assertEquals(
        part2(await parseInput("./test.input.txt")),
        part2TestResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part2(await parseInput("./input.txt")), part2Result);
    });
  });
});
