import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = string[];

export const parseInput = parseInputAsString;

export function part1(input: InputType) {
  const bitArray = numberToBitArray(input);

  const gamma = bitArray.reduce((result, positions) => {
    return result.concat(mostCommonBit(positions).toString());
  }, "");

  const epsilon = bitArray.reduce((result, positions) => {
    return result.concat(leastCommonBit(positions).toString());
  }, "");

  return Number.parseInt(gamma, 2) * Number.parseInt(epsilon, 2);
}

function mostCommonBit(positions: number[]): number {
  const amountOfZeros = positions.filter((v) => v === 0).length;
  const breakpoint = positions.length / 2;
  if (amountOfZeros === breakpoint) {
    return 1;
  }
  return amountOfZeros > breakpoint ? 0 : 1;
}

function leastCommonBit(positions: number[]): number {
  const amountOfZeros = positions.filter((v) => v === 0).length;
  const breakpoint = positions.length / 2;
  if (amountOfZeros === breakpoint) {
    return 0;
  }
  return amountOfZeros < breakpoint ? 0 : 1;
}

function numberToBitArray(input: string[]): number[][] {
  return input.reduce((previous: number[][], current) => {
    for (let index = 0; index < current.length; index++) {
      const element = Number.parseInt(current[index]);
      previous[index] === undefined
        ? (previous[index] = [element])
        : previous[index].push(element);
    }
    return previous;
  }, []);
}

export function part2(input: InputType) {
  const oxigenRating = calculateRating(input, mostCommonBit);
  const co2Rating = calculateRating(input, leastCommonBit);
  return oxigenRating * co2Rating;
}

function calculateRating(
  input: string[],
  searchFunction: (positions: number[]) => number,
): number {
  let remainingInputs = input;
  for (let index = 0; index < input[0].length; index++) {
    const valueToSearch = searchFunction(
      numberToBitArrayInIndex(remainingInputs, index),
    );
    remainingInputs = remainingInputs.filter(
      (s) => Number.parseInt(s[index]) === valueToSearch,
    );

    if (remainingInputs.length === 1) {
      return Number.parseInt(remainingInputs[0], 2);
    }
  }

  throw Error("No result found");
}

function numberToBitArrayInIndex(input: string[], index: number): number[] {
  return input.reduce((previous: number[], current) => {
    const element = Number.parseInt(current[index]);
    previous.push(element);
    return previous;
  }, []);
}
