import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = string;

type PacketCommon = {
  typeId: number;
  version: number;
  bits: string;
  type: "LITERAL" | "NESTED_LENGTH" | "NESTED_AMOUNT";
  remaining?: string;
  subPackets?: Packet[];
};

type PacketLiteral = PacketCommon & {
  type: "LITERAL";
  value: number;
};

type PacketNested = PacketCommon & {
  subPackets: Packet[];
  type: "NESTED_LENGTH" | "NESTED_AMOUNT";
};

type PacketNestedLength = PacketNested & {
  type: "NESTED_LENGTH";
  length: number;
};

type PacketNestedAmount = PacketNested & {
  type: "NESTED_AMOUNT";
  amount: number;
};

type Packet = PacketLiteral | PacketNestedAmount | PacketNestedLength;

export const parseInput = async (path: string) =>
  (await parseInputAsString(path))[0];

export function part1(input: InputType) {
  const bits = input
    .split("")
    .map((n) => Number.parseInt(n, 16).toString(2).padStart(4, "0"))
    .join("");

  const packets = parsePacket(bits);
  return getVersionAddition(packets);
}

const getVersionAddition = (packet: Packet): number => {
  return !packet.subPackets?.length ? packet.version : packet.version +
    packet.subPackets.reduce((acc, p) => acc + getVersionAddition(p), 0);
};

function parsePacket(bits: string): Packet {
  if (!bits || !bits.length) {
    throw new Error("parsePacket for no bits");
  }

  const version = Number.parseInt(bits.substr(0, 3), 2);
  const id = Number.parseInt(bits.substr(3, 3), 2);

  if (id === 4) {
    return parseLiteralValue(version, id, bits);
  }

  const nestedType = bits.substr(6, 1);

  // If the length type ID is 1, then the next 11 bits are a number that represents the number of sub-packets immediately contained by this packet.
  if (nestedType === "1") {
    return parseNestedFromNumberOfPackets(version, id, bits);
  }

  // If the length type ID is 0, then the next 15 bits are a number that represents the total length in bits of the sub-packets contained by this packet.
  if (nestedType === "0") {
    return parseNestedFromLengthInBits(version, id, bits);
  }

  throw new Error("Unknown nested type: " + nestedType);
}

// the next 15 bits are a number that represents the total length in bits of the sub-packets contained by this packet.
function parseNestedFromLengthInBits(
  version: number,
  typeId: number,
  bits: string,
): PacketNestedLength {
  if (!bits) {
    throw new Error("!bits ");
  }

  const rest = bits.substr(7);
  const lengthBits = rest.substr(0, 15);

  if (!lengthBits.length) {
    throw new Error("!packetsLength : " + rest);
  }

  const packetsLength = Number.parseInt(lengthBits, 2);

  const subPacketBits = rest.substr(15, packetsLength);

  const subPackets: Packet[] = [];
  let packet = parsePacket(subPacketBits);
  subPackets.push(packet);

  while (packet.remaining && packet.remaining.length) {
    packet = parsePacket(packet.remaining);
    subPackets.push(packet);
  }

  return {
    bits,
    version,
    typeId,
    type: "NESTED_LENGTH",
    length: packetsLength,
    subPackets,
    remaining: rest.substring(15 + packetsLength),
  };
}

// the next 11 bits are a number that represents the number of sub-packets immediately contained by this packet.
function parseNestedFromNumberOfPackets(
  version: number,
  typeId: number,
  bits: string,
): PacketNestedAmount {
  const rest = bits.substr(7);

  const packetAmount = Number.parseInt(rest.substr(0, 11), 2);

  let subPacketBits: string | undefined = rest.substr(11);
  const packets: Packet[] = [];

  for (let parsedPackets = 0; parsedPackets < packetAmount; parsedPackets++) {
    if (!subPacketBits) {
      throw new Error("Should no be remaining packet but no bits");
    }
    const packet = parsePacket(subPacketBits);
    packets.push(packet);

    subPacketBits = packet.remaining;
  }

  return {
    bits,
    version,
    typeId,
    type: "NESTED_AMOUNT",
    amount: packetAmount,
    remaining: subPacketBits,
    subPackets: packets,
  };
}

function parseLiteralValue(
  version: number,
  typeId: number,
  bits: string,
): PacketLiteral {
  const rest = bits.substr(6);
  let valueBits = "";

  const maxParts = rest.length / 5;
  let remaining = rest;
  for (
    let currentPart = 0;
    currentPart < maxParts && remaining.length;
    currentPart++
  ) {
    const currentValue = remaining.substr(0, 5);
    const leadingBit = currentValue.substr(0, 1);
    valueBits = valueBits + currentValue.substr(1);
    remaining = remaining.substr(5);
    if (leadingBit === "0") {
      break;
    }
  }

  return {
    bits,
    version,
    typeId,
    type: "LITERAL",
    value: Number.parseInt(valueBits, 2),
    remaining,
  };
}

export function part2(input: InputType) {
  const bits = input
    .split("")
    .map((n) => Number.parseInt(n, 16).toString(2).padStart(4, "0"))
    .join("");

  const packet = parsePacket(bits);
  return parsePacketsValue(packet);
}

const parsePacketsValue = (packet?: Packet): number => {
  if (!packet) {
    throw new Error("No packet");
  }

  switch (packet.typeId) {
    case 0:
      return (
        packet.subPackets?.reduce((acc, p) => acc + parsePacketsValue(p), 0) ||
        0
      );
    case 1:
      return (
        packet.subPackets?.reduce((acc, p) => acc * parsePacketsValue(p), 1) ||
        0
      );
    case 2:
      return (
        packet.subPackets?.reduce((acc, p) => {
          const value = parsePacketsValue(p);
          return value < acc ? value : acc;
        }, Number.MAX_VALUE) || 0
      );
    case 3:
      return (
        packet.subPackets?.reduce((acc, p) => {
          const value = parsePacketsValue(p);
          return value > acc ? value : acc;
        }, Number.MIN_VALUE) || 0
      );
    case 4:
      return (packet as PacketLiteral).value;
    case 5: {
      const firstValue = parsePacketsValue(packet.subPackets?.at(0));
      const secondValue = parsePacketsValue(packet.subPackets?.at(1));

      return firstValue > secondValue ? 1 : 0;
    }
    case 6: {
      const firstValue = parsePacketsValue(packet.subPackets?.at(0));
      const secondValue = parsePacketsValue(packet.subPackets?.at(1));

      return firstValue < secondValue ? 1 : 0;
    }
    case 7: {
      const firstValue = parsePacketsValue(packet.subPackets?.at(0));
      const secondValue = parsePacketsValue(packet.subPackets?.at(1));

      return firstValue === secondValue ? 1 : 0;
    }
    default:
      throw new Error("Unknown packet type");
  }
};
