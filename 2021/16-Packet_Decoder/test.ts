import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { describe, it } from "https://deno.land/x/test_suite/mod.ts";
import { parseInput, part1, part2 } from "./src.ts";

const part1Result = 957;
const part2Result = 744953223228;

describe("16 - Packet Decoder", () => {
  describe("Part 1", () => {
    it("Test 1", () => {
      assertEquals(part1("8A004A801A8002F478"), 16);
    });

    it("Test 2", () => {
      assertEquals(part1("620080001611562C8802118E34"), 12);
    });

    it("Test 3", () => {
      assertEquals(part1("C0015000016115A2E0802F182340"), 23);
    });

    it("Test 4", () => {
      assertEquals(part1("A0016C880162017C3686B18A3D4780"), 31);
    });

    it("Input file", async () => {
      assertEquals(part1(await parseInput("./input.txt")), part1Result);
    });
  });

  describe("Part 2", () => {
    it("Test 1", () => {
      assertEquals(part2("C200B40A82"), 3);
    });

    it("Test 2", () => {
      assertEquals(part2("04005AC33890"), 54);
    });

    it("Test 3", () => {
      assertEquals(part2("880086C3E88112"), 7);
    });

    it("Test 4", () => {
      assertEquals(part2("CE00C43D881120"), 9);
    });

    it("Test 5", () => {
      assertEquals(part2("D8005AC2A8F0"), 1);
    });

    it("Test 6", () => {
      assertEquals(part2("F600BC2D8F"), 0);
    });

    it("Test 7", () => {
      assertEquals(part2("9C005AC2F8F0"), 0);
    });

    it("Test 8", () => {
      assertEquals(part2("9C0141080250320F1802104A08"), 1);
    });

    it("Input file", async () => {
      assertEquals(part2(await parseInput("./input.txt")), part2Result);
    });
  });
});
