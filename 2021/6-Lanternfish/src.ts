import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = number[];

export const parseInput = async (path: string) => {
  return (await parseInputAsString(path))[0]
    .split(",")
    .map((n) => Number.parseInt(n));
};

function lanternfishSpawn(input: InputType, loops: number): number {
  let runningFishes = input.reduce((acc, n) => {
    const e = acc.get(n);
    acc.set(n, e ? e + 1 : 1);
    return acc;
  }, new Map<number, number>());

  for (let index = 0; index < loops; index++) {
    const newRunningFishes = new Map<number, number>();
    for (let daysToGo = 0; daysToGo <= 8; daysToGo++) {
      const fishesInThisCase = runningFishes.get(daysToGo);
      if (fishesInThisCase) {
        if (daysToGo === 0) {
          newRunningFishes.set(8, fishesInThisCase);
          newRunningFishes.set(6, fishesInThisCase);
        } else {
          newRunningFishes.set(
            daysToGo - 1,
            (newRunningFishes.get(daysToGo - 1) || 0) + fishesInThisCase,
          );
        }
      }
    }
    runningFishes = newRunningFishes;
  }

  let result = 0;

  runningFishes.forEach((f) => (result = result + f));

  return result;
}

function optimizedSpawn(input: InputType, loops: number): number {
  return lanternfishSpawn(input, loops);
}

export function part1(input: InputType) {
  return optimizedSpawn(input, 80);
}

export function part2(input: InputType) {
  return optimizedSpawn(input, 256);
}
