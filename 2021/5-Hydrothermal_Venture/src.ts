import { parseInputAsString } from "../../utils/deno/input.ts";
type coordinates = number[];
type InputType = Array<coordinates>;

export const parseInput = async (path: string): Promise<InputType> => {
  const input = await parseInputAsString(path);
  return input.map((l) => {
    return l
      .split(" -> ")
      .map((p) => p.split(","))
      .reduce((c: coordinates, a) => {
        return c.concat(a.map((n) => Number.parseInt(n)));
      }, []);
  });
};
export function part1(input: InputType) {
  const matrix = input
    .map(([xStart, yStart, xEnd, yEnd]) => {
      const coordinates: Array<[number, number]> = [];
      if (xStart === xEnd) {
        const fixed = xStart;
        const end = Math.max(yStart, yEnd);
        const start = Math.min(yStart, yEnd);

        for (let index = start; index <= end; index++) {
          coordinates.push([fixed, index]);
        }
      } else if (yStart === yEnd) {
        const fixed = yStart;
        const end = Math.max(xStart, xEnd);
        const start = Math.min(xStart, xEnd);

        for (let index = start; index <= end; index++) {
          coordinates.push([index, fixed]);
        }
      }

      return coordinates;
    })
    .reduce((winds: Map<string, number>, line) => {
      line.forEach(([x, y]) => {
        const coordinate = `${x}-${y}`;
        const existing = winds.get(coordinate);
        if (existing) {
          winds.set(coordinate, existing + 1);
        } else {
          winds.set(coordinate, 1);
        }
      });
      return winds;
    }, new Map());

  let graterThantTwo = 0;
  matrix.forEach((e) => {
    if (e > 1) {
      graterThantTwo++;
    }
  });

  return graterThantTwo;
}

export function part2(input: InputType) {
  const matrix = input
    .map(([xStart, yStart, xEnd, yEnd]) => {
      const coordinates: Array<[number, number]> = [];
      if (xStart === xEnd) {
        const fixed = xStart;
        const end = Math.max(yStart, yEnd);
        const start = Math.min(yStart, yEnd);

        for (let index = start; index <= end; index++) {
          coordinates.push([fixed, index]);
        }
      } else if (yStart === yEnd) {
        const fixed = yStart;
        const end = Math.max(xStart, xEnd);
        const start = Math.min(xStart, xEnd);

        for (let index = start; index <= end; index++) {
          coordinates.push([index, fixed]);
        }
      } else {
        const end = Math.max(xStart, xEnd);
        const start = Math.min(xStart, xEnd);
        const difference = end - start;
        for (let index = 0; index <= difference; index++) {
          coordinates.push([
            xStart < xEnd ? xStart + index : xStart - index,
            yStart < yEnd ? yStart + index : yStart - index,
          ]);
        }
      }

      return coordinates;
    })
    .reduce((winds: Map<string, number>, line) => {
      line.forEach(([x, y]) => {
        const coordinate = `${x}-${y}`;
        const existing = winds.get(coordinate);
        if (existing) {
          winds.set(coordinate, existing + 1);
        } else {
          winds.set(coordinate, 1);
        }
      });
      return winds;
    }, new Map());

  let graterThantTwo = 0;
  matrix.forEach((e) => {
    if (e > 1) {
      graterThantTwo++;
    }
  });

  return graterThantTwo;
}
