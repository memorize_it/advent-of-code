import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import { describe, it } from "https://deno.land/x/test_suite/mod.ts";
import { parseInput, part1, part2 } from "./src.ts";

const part1TestResult = 5;
const part1Result = 7318;
const part2TestResult = 12;
const part2Result = 0;

describe("5 - Hydrothermal Venture", () => {
  describe("Part 1", () => {
    it("Test file", async () => {
      assertEquals(
        part1(await parseInput("./test.input.txt")),
        part1TestResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part1(await parseInput("./input.txt")), part1Result);
    });
  });

  describe("Part 2", () => {
    it("Test file", async () => {
      assertEquals(
        part2(await parseInput("./test.input.txt")),
        part2TestResult,
      );
    });

    it("Input file", async () => {
      assertEquals(part2(await parseInput("./input.txt")), part2Result);
    });
  });
});
