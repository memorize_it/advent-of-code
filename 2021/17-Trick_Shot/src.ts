import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = number[][];

export const parseInput = async (path: string): Promise<InputType> => {
  const input = await parseInputAsString(path);
  return input[0]
    .replace("target area: ", "")
    .split(", ")
    .map((s) =>
      s
        .split("=")[1]
        .split("..")
        .map((n) => Number.parseInt(n))
        .sort((a, b) => a - b)
    );
};

export function part1(input: InputType) {
  const [_, [yMin]] = input;
  const dy = yMin * -1;
  return (dy * (dy - 1)) / 2;
}

export function part2(input: InputType) {
  let count = 0;
  for (let xAccelleration = -200; xAccelleration <= 200; xAccelleration++) {
    for (let yAcceleration = -200; yAcceleration <= 200; yAcceleration++) {
      if (isValidThrow(xAccelleration, yAcceleration, input)) {
        count++;
      }
    }
  }
  return count;
}

const isValidThrow = (
  xAcceleration: number,
  yAcceleration: number,
  [[xMin, xMax], [yMin, yMax]]: InputType,
): boolean => {
  let turn: Turn = {
    x: 0,
    y: 0,
    xAcceleration,
    yAcceleration,
  };
  while (true) {
    turn = runTurn(turn);
    const { x, y } = turn;
    if (x > xMax || y < yMin) {
      return false;
    }

    if (x >= xMin && x <= xMax && y >= yMin && y <= yMax) {
      return true;
    }
  }
};

interface Turn {
  x: number;
  y: number;
  xAcceleration: number;
  yAcceleration: number;
}

const runTurn = ({ x, y, xAcceleration, yAcceleration }: Turn): Turn => {
  return {
    x: x + xAcceleration,
    y: y + yAcceleration,
    xAcceleration: xAcceleration > 0
      ? xAcceleration - 1
      : xAcceleration < 0
      ? xAcceleration + 1
      : 0,
    yAcceleration: yAcceleration - 1,
  };
};
