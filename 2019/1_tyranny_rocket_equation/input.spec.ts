import { parseInput, part1, part2 } from "./src";

const inputFile = "./2019/1_tyranny_rocket_equation/input.txt";
const testFile = "./2019/1_tyranny_rocket_equation/test.input.txt";

xdescribe("1 - The Tyranny of the Rocket Equation", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 33583", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(33583);
      });
    });

    describe("Input file", () => {
      it("Should return 3297896", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(3297896);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 50346", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(50346);
      });
    });

    describe("Input file", () => {
      it("Should return 4943969", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(4943969);
      });
    });
  });
});
