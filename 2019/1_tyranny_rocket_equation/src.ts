import { parseInputAsArray } from "../../utils/input";

type InputType = number[];

export async function parseInput(path: string): Promise<InputType> {
  const input = (await parseInputAsArray(path)).map((n) => Number.parseInt(n));
  return input;
}

export function part1(input: InputType) {
  return input.reduce((c, mass) => {
    const result = calculateFuel(mass);
    return c + result;
  }, 0);
}

export function part2(input: InputType) {
  return input.reduce((c, mass) => {
    let result = calculateFuel(mass);
    let prevFuel = result;
    while (prevFuel > 0) {
      prevFuel = calculateFuel(prevFuel);
      result += prevFuel > 0 ? prevFuel : 0;
    }
    return c + result;
  }, 0);
}

function calculateFuel(mass: number) {
  return Math.floor(mass / 3) - 2;
}
