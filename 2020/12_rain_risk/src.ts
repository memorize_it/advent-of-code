import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

type Coordinate = "N" | "S" | "E" | "W";
type Direction = Coordinate | "L" | "R" | "F";

type InputType = Array<[Direction, number]>;

export async function parseInput(path: string): Promise<InputType> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: InputType = [];
      readInterface.on("line", (line) => {
        result.push([line[0] as Direction, Number.parseInt(line.slice(1))]);
      });

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

export function part1(input: InputType) {
  let currentCoordinate: Coordinate = "E";
  const currentPosition = {
    E: 0,
    N: 0,
  };
  for (let i = 0; i < input.length; i++) {
    const [direction, amount] = input[i];

    switch (direction) {
      case "N":
        currentPosition.N += amount;
        break;
      case "S":
        currentPosition.N -= amount;
        break;
      case "E":
        currentPosition.E += amount;
        break;
      case "W":
        currentPosition.E -= amount;
        break;
      case "R":
        switch (currentCoordinate) {
          case "N":
            switch (amount) {
              case 90:
                currentCoordinate = "E";
                break;
              case 180:
                currentCoordinate = "S";
                break;
              case 270:
                currentCoordinate = "W";
                break;
            }
            break;
          case "S":
            switch (amount) {
              case 90:
                currentCoordinate = "W";
                break;
              case 180:
                currentCoordinate = "N";
                break;
              case 270:
                currentCoordinate = "E";
                break;
            }
            break;
          case "E":
            switch (amount) {
              case 90:
                currentCoordinate = "S";
                break;
              case 180:
                currentCoordinate = "W";
                break;
              case 270:
                currentCoordinate = "N";
                break;
            }
            break;
          case "W":
            switch (amount) {
              case 90:
                currentCoordinate = "N";
                break;
              case 180:
                currentCoordinate = "E";
                break;
              case 270:
                currentCoordinate = "S";
                break;
            }
            break;
        }
        break;
      case "L":
        switch (currentCoordinate) {
          case "N":
            switch (amount) {
              case 90:
                currentCoordinate = "W";
                break;
              case 180:
                currentCoordinate = "S";
                break;
              case 270:
                currentCoordinate = "E";
                break;
            }
            break;
          case "S":
            switch (amount) {
              case 90:
                currentCoordinate = "E";
                break;
              case 180:
                currentCoordinate = "N";
                break;
              case 270:
                currentCoordinate = "W";
                break;
            }
            break;
          case "E":
            switch (amount) {
              case 90:
                currentCoordinate = "N";
                break;
              case 180:
                currentCoordinate = "W";
                break;
              case 270:
                currentCoordinate = "S";
                break;
            }
            break;
          case "W":
            switch (amount) {
              case 90:
                currentCoordinate = "S";
                break;
              case 180:
                currentCoordinate = "E";
                break;
              case 270:
                currentCoordinate = "N";
                break;
            }
            break;
        }
        break;
      case "F":
        switch (currentCoordinate) {
          case "N":
            currentPosition.N += amount;
            break;
          case "S":
            currentPosition.N -= amount;
            break;
          case "E":
            currentPosition.E += amount;
            break;
          case "W":
            currentPosition.E -= amount;
            break;
        }
        break;
    }
  }

  return Math.abs(currentPosition.E) + Math.abs(currentPosition.N);
}

export function part2(input: InputType) {
  const currentPosition = {
    E: 0,
    N: 0,
  };

  let waypoint = {
    E: 10,
    N: 1,
  };

  for (let i = 0; i < input.length; i++) {
    const [direction, amount] = input[i];

    switch (direction) {
      case "N":
        waypoint.N += amount;
        break;
      case "S":
        waypoint.N -= amount;
        break;
      case "E":
        waypoint.E += amount;
        break;
      case "W":
        waypoint.E -= amount;
        break;
      case "L":
      case "R":
        if (amount === 180) {
          waypoint = {
            E: -waypoint.E,
            N: -waypoint.N,
          };
          break;
        }

        if (
          (direction === "L" && amount === 90) ||
          (direction === "R" && amount === 270)
        ) {
          waypoint = {
            E: -waypoint.N,
            N: waypoint.E,
          };
          break;
        }

        waypoint = {
          E: waypoint.N,
          N: -waypoint.E,
        };
        break;
      case "F":
        currentPosition.N += amount * waypoint.N;
        currentPosition.E += amount * waypoint.E;
        break;
    }
  }

  return Math.abs(currentPosition.E) + Math.abs(currentPosition.N);
}
