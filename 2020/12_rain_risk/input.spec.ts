import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/12_rain_risk/input.txt";
const testFile = "./2020/12_rain_risk/test.input.txt";

xdescribe("12 - Rain Risk", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 25", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(25);
      });
    });

    describe("Input file", () => {
      it("Should return 582", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(582);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 286", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(286);
      });
    });

    describe("Input file", () => {
      it("Should return 52069", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(52069);
      });
    });
  });
});
