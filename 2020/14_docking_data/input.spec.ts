import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/14_docking_data/input.txt";
const testFile = "./2020/14_docking_data/test.input.txt";
const test2File = "./2020/14_docking_data/test2.input.txt";

xdescribe("14 - Docking Data", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 165", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(165);
      });
    });

    describe("Input file", () => {
      it("Should return 4886706177792", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(4886706177792);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 208", async () => {
        const input = await parseInput(test2File);
        expect(part2(input)).toEqual(208);
      });
    });

    describe("Input file", () => {
      it("Should return 3348493585827", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(3348493585827);
      });
    });
  });
});
