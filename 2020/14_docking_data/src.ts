import { parseInputAsArray } from "../../utils/input";

type MemoryUpdate = {
  address: number;
  value: number;
};

type Program = {
  mask: string;
  operations: Array<MemoryUpdate>;
};

type InputType = Program[];

export async function parseInput(path: string): Promise<InputType> {
  const input = await parseInputAsArray(path);

  const result = [];
  let p: Program | undefined;
  for (let i = 0; i < input.length; i++) {
    const current = input[i];
    const mask = current.split("mask = ")[1];

    if (mask) {
      if (p) {
        result.push(p);
      }

      p = {
        mask,
        operations: [],
      };
      continue;
    }

    const [mem, value] = current.split(" = ");
    p.operations.push({
      address: Number.parseInt(mem.match(/^mem\[([0-9]+)\]/)[1]),
      value: Number.parseInt(value),
    });
  }

  result.push(p);
  return result;
}

export function part1(input: InputType) {
  const mem: number[] = [];
  input.forEach(({ mask, operations }) => {
    operations.forEach(({ address, value }) => {
      const binaryValue = (value >>> 0)
        .toString(2)
        .split("")
        .map((c) => (c ? Number.parseInt(c) : 0));

      while (binaryValue.length < mask.length) {
        binaryValue.unshift(0);
      }

      for (let i = 0; i < mask.length; i++) {
        const m = mask[i];

        if (m !== "X") {
          binaryValue[i] = Number.parseInt(m);
        }
      }

      mem[address] = Number.parseInt(binaryValue.join(""), 2);
    });
  });

  return mem.reduce((c, v) => (v ? c + v : c), 0);
}

function allPossibilitiesBinary(binaryAddress: string[]): string[] {
  const floatingIndex = binaryAddress.findIndex((c) => c === "X");

  if (floatingIndex < 0) {
    return [binaryAddress.join("")];
  }

  const fixedPart = binaryAddress.slice(0, floatingIndex).join("");
  const floatingPart = binaryAddress.slice(floatingIndex, binaryAddress.length);

  return ["0", "1"].reduce((current, i) => {
    floatingPart[0] = i;
    current.push(
      ...allPossibilitiesBinary(floatingPart).map((c) =>
        [fixedPart, c].join("")
      )
    );
    return current;
  }, []);
}

export function part2(input: InputType) {
  const mem: { [key: number]: number } = {};

  input.forEach(({ mask, operations }) => {
    operations.forEach(({ address, value }) => {
      const binaryAddress = address.toString(2).padStart(36, "0").split("");

      for (let i = 0; i < mask.length; i++) {
        const m = mask[i];

        if (m !== "0") {
          binaryAddress[i] = m;
        }
      }

      const newAddresses = allPossibilitiesBinary(binaryAddress).map((v) =>
        Number.parseInt(v, 2)
      );

      newAddresses.forEach((location) => (mem[location] = value));
    });
  });

  return Object.getOwnPropertyNames(mem)
    .map((val) => mem[val])
    .reduce((result, val) => (result += val), 0);
}
