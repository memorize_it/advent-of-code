import { parseInputAsArray } from "../../utils/input";

type Rule = string | number[] | number[][];
type Rules = Map<number, Rule>;
type DeveloppedRules = Map<string, RegExp>;
type InputType = {
  rules: Rules;
  messages: string[];
};

export async function parseInput(path: string): Promise<InputType> {
  const input = await parseInputAsArray(path);
  const rules: Rules = new Map();
  const messages: string[] = [];

  let rulesFinished = false;
  for (let index = 0; index < input.length; index++) {
    const element = input[index];

    if (element === "") {
      rulesFinished = true;
    } else if (!rulesFinished) {
      const [key, rest] = element.split(": ");
      let value: Rule;
      if (rest.includes('"')) {
        value = rest.split('"')[1];
      } else if (rest.includes("|")) {
        value = rest.split("|").map((numbers) =>
          numbers
            .split(" ")
            .filter(Boolean)
            .map((n) => Number.parseInt(n))
        );
      } else {
        value = rest
          .split(" ")
          .filter(Boolean)
          .map((n) => Number.parseInt(n));
      }
      rules.set(Number.parseInt(key), value);
    } else {
      messages.push(element);
    }
  }

  return {
    rules,
    messages,
  };
}

function toKey(n: number[]): string {
  return n.join("_");
}

function searchInRules(
  ruleKey: number,
  rules: Rules,
  developpedRules: DeveloppedRules = new Map()
): RegExp {
  const devKey = toKey([ruleKey]);
  if (developpedRules.has(devKey)) {
    return developpedRules.get(devKey);
  }

  const rule = rules.get(ruleKey);
  if (typeof rule === "string") {
    const regExp: RegExp = new RegExp(`${rule}`);
    developpedRules.set(devKey, regExp);
    return regExp;
  }

  let regExp: RegExp;
  if (Number.isInteger(rule[0])) {
    const numberOnlyRule: number[] = rule as number[];
    regExp = searchInDeveloppedRules(
      ruleKey,
      numberOnlyRule,
      rules,
      developpedRules
    );
  } else {
    const arrayOfNumberOnlyRules: number[][] = rule as number[][];
    regExp = combineRegExps(
      arrayOfNumberOnlyRules.map((numberOnlyRule) =>
        searchInDeveloppedRules(ruleKey, numberOnlyRule, rules, developpedRules)
      ),
      "|"
    );
  }

  developpedRules.set(devKey, regExp);
  return regExp;
}

function searchInDeveloppedRules(
  initialKey: number,
  rule: number[],
  rules: Rules,
  developpedRules: DeveloppedRules
): RegExp {
  const developpedKey = toKey(rule);
  if (developpedRules.has(developpedKey)) {
    return developpedRules.get(developpedKey);
  }

  const recursiveIndex = rule.indexOf(initialKey);
  let regExp: RegExp;

  if (recursiveIndex < 0) {
    const parts: RegExp[] = rule.map((r) =>
      searchInRules(r, rules, developpedRules)
    );

    regExp = combineRegExps(parts);
  } else {
    const beforeKey = toKey(rule.slice(0, recursiveIndex));
    const before: RegExp = developpedRules.get(beforeKey);

    const afterKey = toKey(rule.slice(recursiveIndex + 1, rule.length));
    const after: RegExp = developpedRules.get(afterKey);

    for (let index = 0; index < 5; index++) {
      const array: RegExp[] = [];

      if (before) {
        array.push(before);
      }

      if (regExp) {
        array.push(regExp);
      }

      if (after) {
        array.push(after);
      }

      regExp = regExp
        ? combineRegExps([regExp, combineRegExps(array)], "|")
        : combineRegExps(array);
    }
  }

  developpedRules.set(developpedKey, regExp);
  return regExp;
}

function combineRegExps(parts: RegExp[], separator: string = "") {
  return new RegExp(`(${parts.map(({ source }) => source).join(separator)})`);
}

export function part1({ rules, messages }: InputType) {
  const regExp = new RegExp(`^${searchInRules(0, rules).source}$`, "gm");
  return messages.filter((m) => m.match(regExp)).length;
}

export function part2(input: InputType) {
  return part1(input);
}
