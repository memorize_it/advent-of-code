import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/19_monster_messages/input.txt";
const inputFile2 = "./2020/19_monster_messages/input2.txt";
const testFile = "./2020/19_monster_messages/test.input.txt";
const testFile2 = "./2020/19_monster_messages/test2.input.txt";
const testFile3 = "./2020/19_monster_messages/test3.input.txt";

xdescribe("19 - Monster Messages", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 2", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(2);
      });
    });

    describe("Test file 2", () => {
      it("Should return 3", async () => {
        const input = await parseInput(testFile2);
        expect(part1(input)).toEqual(3);
      });
    });

    describe("Input file", () => {
      it("Should return 248", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(248);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 12", async () => {
        const input = await parseInput(testFile3);
        expect(part2(input)).toEqual(12);
      });
    });

    describe("Input file", () => {
      it("Should return 381", async () => {
        const input = await parseInput(inputFile2);
        expect(part2(input)).toEqual(381);
      });
    });
  });
});
