import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

export type Map = Array<Array<string>>;
export type Slope = Array<[number, number]>;

export async function parseInput(path: string): Promise<Map> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: Map = [];
      let row = 0;

      readInterface.on("line", (line) => {
        result[row] = line.split("");
        row++;
      });

      readInterface.on("close", () => {
        res(result);
      });
    } catch (error) {
      rej(error);
    }
  });
}

export function rideMap(m: Map, right: number, down: number) {
  let count = 0;

  for (let x = 0, y = 0; y < m.length - 1; ) {
    x += right;
    y += down;
    if (m[y][x % m[0].length] === "#") count++;
  }
  return count;
}

export function multiRide(m: Map, slopes: Slope) {
  return slopes.reduce((c, [x, y]) => c * rideMap(m, x, y), 1);
}
