import { Slope, parseInput, rideMap, multiRide } from "./src";

xdescribe("3 - Toboggan Trajectory", () => {
  const inputFile = "./2020/3_toboggan_trajectory/input.txt";
  const testFile = "./2020/3_toboggan_trajectory/test.input.txt";

  const testInput = [
    [".", ".", "#", "#", ".", ".", ".", ".", ".", ".", "."],
    ["#", ".", ".", ".", "#", ".", ".", ".", "#", ".", "."],
    [".", "#", ".", ".", ".", ".", "#", ".", ".", "#", "."],
    [".", ".", "#", ".", "#", ".", ".", ".", "#", ".", "#"],
    [".", "#", ".", ".", ".", "#", "#", ".", ".", "#", "."],
    [".", ".", "#", ".", "#", "#", ".", ".", ".", ".", "."],
    [".", "#", ".", "#", ".", "#", ".", ".", ".", ".", "#"],
    [".", "#", ".", ".", ".", ".", ".", ".", ".", ".", "#"],
    ["#", ".", "#", "#", ".", ".", ".", "#", ".", ".", "."],
    ["#", ".", ".", ".", "#", "#", ".", ".", ".", ".", "#"],
    [".", "#", ".", ".", "#", ".", ".", ".", "#", ".", "#"],
  ];

  describe("Part 1", () => {
    describe("Test file", () => {
      it("Must parse read the result", async () => {
        const testInputResult = await parseInput(testFile);
        expect(testInputResult).toEqual(testInput);
      });

      it("Should return 7", () => {
        expect(rideMap(testInput, 3, 1)).toEqual(7);
      });
    });

    describe("Input file", () => {
      it("Should return 173", async () => {
        const input = await parseInput(inputFile);
        expect(rideMap(input, 3, 1)).toEqual(173);
      });
    });
  });

  describe("Part 2", () => {
    const slopes: Slope = [
      [1, 1],
      [3, 1],
      [5, 1],
      [7, 1],
      [1, 2],
    ];

    describe("Test file", () => {
      it("Should return 336", () => {
        expect(multiRide(testInput, slopes)).toEqual(336);
      });
    });

    describe("Input file", () => {
      it("Should return 4385176320", async () => {
        const input = await parseInput(inputFile);
        expect(multiRide(input, slopes)).toEqual(4385176320);
      });
    });
  });
});
