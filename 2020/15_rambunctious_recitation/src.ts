import { parseInputAsArray } from "../../utils/input";

type InputType = number[];

export async function parseInput(path: string): Promise<InputType> {
  const input = (await parseInputAsArray(path))[0]
    .split(",")
    .map((c) => Number.parseInt(c));
  return input;
}

export function part1(input: InputType, turns: number = 2020) {
  const spoken: Map<number, number> = new Map();

  input.forEach((n, i) => spoken.set(n, i + 1));

  let lastSpoken: number = input[input.length - 1];

  for (
    let currentTurn = input.length + 1;
    currentTurn <= turns;
    currentTurn++
  ) {
    let newSpoken = spoken.has(lastSpoken)
      ? currentTurn - 1 - spoken.get(lastSpoken)
      : 0;

    spoken.set(lastSpoken, currentTurn - 1);
    lastSpoken = newSpoken;
  }

  return lastSpoken;
}

export function part2(input: InputType) {
  return part1(input, 30000000);
}
