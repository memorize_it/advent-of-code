import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/15_rambunctious_recitation/input.txt";
const testFile = "./2020/15_rambunctious_recitation/test.input.txt";

xdescribe("15 - Rambunctious Recitation", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 436", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(436);
      });
    });

    describe("Input file", () => {
      it("Should return 260", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(260);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 175594", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(175594);
      });
    });

    describe("Input file", () => {
      it("Should return 67587168", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(67587168);
      });
    });
  });
});
