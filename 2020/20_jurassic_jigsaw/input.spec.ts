import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/20_jurassic_jigsaw/input.txt";
const testFile = "./2020/20_jurassic_jigsaw/test.input.txt";

describe("20 - Jurassic Jigsaw", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 20899048083289", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(20899048083289);
      });
    });

    describe("Input file", () => {
      it("Should return 104831106565027", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(104831106565027);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 273", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(273);
      });
    });

    describe("Input file", () => {
      it("Should return 2093", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(2093);
      });
    });
  });
});
