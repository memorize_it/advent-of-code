import { Grid, Index } from "./grid";

export class Canvas {
  private _grid: Grid;

  constructor(grid: string[]) {
    this._grid = new Grid(grid);
  }

  public countCharacter(character = "#"): number {
    return this._grid.countCharacter(character);
  }

  public replace(pattern: string[]) {
    const checks: Index[] = pattern.reduce(
      (c: Index[], l: string, line: number) => [
        ...c,
        ...l
          .split("")
          .map((character, column): Index | undefined =>
            character === "#"
              ? {
                  line,
                  column,
                }
              : undefined
          )
          .filter(Boolean),
      ],
      []
    );

    let result = new Grid(this._grid.getGrid());
    const width = pattern[0].length;
    const height = pattern.length;

    let needToRotate = true;
    let i = 0;
    while (needToRotate && i < 8) {
      const maxRows = result.rows();
      const maxColumns = result.columns();

      for (let lineIndex = 0; lineIndex <= maxRows - height; lineIndex++) {
        for (
          let columnIndex = 0;
          columnIndex <= maxColumns - width;
          columnIndex++
        ) {
          let seaMonsterMatch = false;

          const indexes = checks.map(
            ({ line, column }): Index => ({
              line: lineIndex + line,
              column: columnIndex + column,
            })
          );

          if (indexes.every((index) => result.characterIs(index, "#"))) {
            seaMonsterMatch = true;
            indexes.forEach((index) => {
              result.replaceInPosition(index, "O");
            });
          }

          if (seaMonsterMatch) {
            needToRotate = false;
          }
        }
      }

      if (needToRotate) {
        if (i === 3) {
          result.flipHorizontal();
        } else {
          result.rotateClockWise();
        }
      }

      i++;
    }

    this._grid = result;
  }
}
