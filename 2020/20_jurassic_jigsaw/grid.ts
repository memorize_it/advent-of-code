export interface Index {
  line: number;
  column: number;
}

export class Grid {
  private _grid: string[];

  constructor(grid: string[]) {
    this._grid = grid;
  }

  public getGrid(): string[] {
    return [...this._grid];
  }

  public characterIs({ line, column }: Index, match: string): boolean {
    return this._grid[line][column] === match;
  }

  public countCharacter(character = "#"): number {
    return this._grid
      .map((l) => l.split("").filter((s) => s === character).length)
      .reduce((c, i) => c + i, 0);
  }

  public rows() {
    return this._grid.length;
  }

  public columns() {
    return this._grid.length ? this._grid[0].length : 0;
  }

  public flipHorizontal(): void {
    this._grid = this._grid.map((line) => this._reverse(line));
  }

  public flipVetical() {
    this._grid = this._grid.reverse();
  }

  public removeBorder() {
    this._grid = this._grid.slice(1, this._grid.length - 1);
    this._grid = this._grid.map((l) => l.slice(1, l.length - 1));
  }

  public replaceInPosition({ line, column }: Index, replacement: string) {
    this._grid[line] = this._replaceInString(
      this._grid[line],
      column,
      replacement
    );
  }

  public rotateClockWise(): void {
    const canvas = this._grid;
    const result: string[] = [];

    for (let columnIndex = 0; columnIndex < canvas[0].length; columnIndex++) {
      let line = "";
      for (let lineIndex = 0; lineIndex < canvas.length; lineIndex++) {
        line = canvas[lineIndex][columnIndex] + line;
      }
      result[columnIndex] = line;
    }

    this._grid = result;
  }

  public removeFirstRow() {
    return (this._grid = this._grid.slice(1, this._grid.length));
  }

  public removeFirstColumn() {
    return this._grid.map((r) => r.slice(1, r.length));
  }

  public combineGrid(g: string[]) {
    return this._grid.map((l, i) => {
      return (this._grid[i] = l + g[i]);
    });
  }

  public getRightBorder() {
    return this._grid.reduce((c, l) => (c += l[l.length - 1]), "");
  }

  public getBottomBorder() {
    return this._grid[this._grid.length - 1];
  }

  public rotate(i: number) {
    if (i === 3) {
      this.flipHorizontal();
      return;
    }
    if (i < 7) {
      this.rotateClockWise();
      return;
    }

    throw new Error("Rotated too many times");
  }

  public toString() {
    return this._grid.join("\n");
  }

  private _reverse(line: string): string {
    return line.split("").reverse().join("");
  }

  private _replaceInString(
    input: string,
    index: number,
    replacement: string
  ): string {
    return [
      input.substr(0, index),
      replacement,
      input.substr(index + replacement.length),
    ].join("");
  }
}
