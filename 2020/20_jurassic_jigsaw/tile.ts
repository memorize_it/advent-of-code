import { Grid } from "./grid";

interface Border {
  borderId: string;
  border: string;
  reverseBorder: string;
  match?: string;
  matchRotation?: number;
  matchFlip?: boolean;
}

export interface TileInterface {
  id: number;
  grid: string[];
}

export class Tile {
  public id: number;
  private _borders: Border[];
  private _grid: Grid;

  constructor({ id, grid }: TileInterface) {
    const firstBorder = {
      borderId: [id, 0].join("_"),
      border: grid[0],
    };

    const thirdBorder = {
      borderId: [id, 2].join("_"),
      border: grid[grid.length - 1],
    };

    const secondBorder = {
      borderId: [id, 1].join("_"),
      border: "",
    };
    const fourthBorder = {
      borderId: [id, 3].join("_"),
      border: "",
    };

    for (let index = 0; index < grid.length; index++) {
      const line = grid[index];
      fourthBorder.border += line[0];
      secondBorder.border += line[line.length - 1];
    }

    this.id = id;
    this._grid = new Grid(grid);
    this._borders = [firstBorder, secondBorder, thirdBorder, fourthBorder].map(
      (b) => ({
        ...b,
        reverseBorder: this._reverse(b.border),
      })
    );
  }

  public print(){
    console.log([
      `Tile: ${this.id}`,
      this._grid.toString(),
    ].join('\n'))
  }

  public getGrid() {
    return this._grid.getGrid();
  }

  public removeFirstRow() {
    return this._grid.removeFirstRow();
  }

  public removeFirstColumn() {
    return this._grid.removeFirstColumn();
  }

  public combineTile(t: Tile) {
    return this._grid.combineGrid(t.removeFirstColumn());
  }

  public getRightBorder() {
    return this._grid.getRightBorder();
  }

  public getBottomBorder() {
    return this._grid.getBottomBorder();
  }

  public includesBoder(b: string) {
    return !!this._borders.find(
      ({ border, reverseBorder }) => border === b || reverseBorder === b
    );
  }

  public rotate(){
    this._grid.rotate(1);
  }

  public positionLeftBorder(b: string) {
    let i = 0;
    while (this.getRightBorder() !== b) {
      this._grid.rotate(i);
      i++;
    }

    this._grid.flipHorizontal();
  }

  public positionUpperBorder(b: string) {
    let i = 0;
    while (this.getBottomBorder() !== b) {
      this._grid.rotate(i);
      i++;
    }

    this._grid.flipVetical();
  }

  public removeBorder(){
    this._grid.removeBorder();
    return this.getGrid();
  }

  public hasEmptyBorders(count: number) {
    return this._borders.filter(({ match }) => !match).length === count;
  }

  public isCorner(): boolean {
    return this.hasEmptyBorders(2);
  }

  public isUpperLeft(): boolean {
    const borders = this._borders;
    return !borders[0].match && !this._borders[3].match;
  }

  public matchBorders(other: Tile) {
    return this._borders.forEach((iB) => {
      const { border: iBorder, borderId: iId, reverseBorder: iReverse } = iB;
      other._borders.forEach((kB) => {
        const { borderId: kId, border: kBorder } = kB;
        const borderMatch = kBorder === iBorder || kBorder === iReverse;
        if (borderMatch) {
          iB.match = kId;
          kB.match = iId;
          if (kBorder === iReverse) {
            iB.matchFlip = true;
            kB.matchFlip = true;
          }
        }
      });
    });
  }

  private _reverse(line: string): string {
    return line.split("").reverse().join("");
  }
}
