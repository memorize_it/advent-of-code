import { writeFileSync, existsSync, readFileSync } from "fs";
import { parseInputAsArray } from "../../utils/input";
import { Tile, TileInterface } from "./tile";
import { Canvas } from "./canvas";
import { Grid } from "./grid";

type TileMatrix = Tile[][];

type InputType = TileInterface[];

export async function parseInput(path: string): Promise<InputType> {
  const processedPath = `${path}.processed.json`;
  if (existsSync(processedPath)) {
    return JSON.parse(readFileSync(processedPath).toString());
  }

  const input: string[] = await parseInputAsArray(path);

  const tiles = [];
  let tile: TileInterface = {
    id: 0,
    grid: [],
  };

  for (let i = 0; i < input.length; i++) {
    const line = input[i];
    if (line === "") {
      tiles.push(tile);
      tile = {
        id: 0,
        grid: [],
      };
    } else if (line.includes("Tile")) {
      const id = line.match(new RegExp("Tile ([0-9]+):"))[1];
      tile.id = Number.parseInt(id);
    } else {
      tile.grid.push(line);
    }
  }

  tiles.push(tile);

  writeFileSync(processedPath, JSON.stringify(tiles));
  return tiles;
}

export function part1(input: InputType) {
  return loadTilesWithBorders(input)
    .filter((t) => t.hasEmptyBorders(2))
    .map(({ id }) => id)
    .reduce((c, i) => c * i, 1);
}

function loadTilesWithBorders(input: InputType): Tile[] {
  const tiles: Tile[] = input.map((tile) => new Tile(tile));

  for (let i = 0; i < tiles.length; i++) {
    const main = tiles[i];
    for (let k = i + 1; k < tiles.length; k++) {
      main.matchBorders(tiles[k]);
    }
  }

  return tiles;
}

function reduceTileGrid(matrix: TileMatrix): string[] {
  return matrix.reduce((c: string[], tileRow): string[] => {
    const completeRow = tileRowToArray(tileRow);
    c.push(...completeRow);
    return c;
  }, []);
}

function tileRowToArray(tileRow: Tile[]): string[] {
  const tilesWithoutBorder = tileRow.map((t) => t.removeBorder());
  return tilesWithoutBorder
    .slice(1, tilesWithoutBorder.length)
    .reduce((row: Grid, g: string[]) => {
      row.combineGrid(g);
      return row;
    }, new Grid(tilesWithoutBorder[0]))
    .getGrid();
}

function removeTile(tiles: Tile[], currentTile: Tile): Tile[] {
  const index = tiles.findIndex(({ id }) => id === currentTile.id);

  if (index < 0) {
    throw new Error("Tile not exist");
  }

  return [...tiles.slice(0, index), ...tiles.slice(index + 1, tiles.length)];
}

function getCanvas(input: InputType): string[] {
  let tiles = loadTilesWithBorders(input);
  let currentTile = tiles.find((t) => t.isCorner());

  if(!currentTile.isUpperLeft()){
    currentTile.rotate();
    currentTile.rotate();
    currentTile.rotate();
  }

  tiles = removeTile(tiles, currentTile);
  const finalGrid: TileMatrix = [[currentTile]];
  let line = 0;

  const length = tiles.length;
  for (let tileNumber = 0; tileNumber < length; tileNumber++) {
    const rightBorder = currentTile.getRightBorder();
    let nextTile = tiles.find((tile) => tile.includesBoder(rightBorder));

    if (nextTile) {
      nextTile.positionLeftBorder(rightBorder);
      finalGrid[line].push(nextTile);
    } else {
      currentTile = finalGrid[line][0];
      line++;

      const bottomBorder = currentTile.getBottomBorder();
      nextTile = tiles.find((tile) => tile.includesBoder(bottomBorder));

      if (!nextTile) {
        throw new Error("Missing tiles");
      }

      nextTile.positionUpperBorder(bottomBorder);
      finalGrid.push([nextTile]);
    }

    tiles = removeTile(tiles, nextTile);
    currentTile = nextTile;
  }

  return reduceTileGrid(finalGrid);
}

export function part2(input: InputType) {
  const grid: string[] = getCanvas(input);

  const canvas = new Canvas(grid);

  const seaMonster = [
    "                  # ",
    "#    ##    ##    ###",
    " #  #  #  #  #  #   ",
  ];

  canvas.replace(seaMonster);

  return canvas.countCharacter();
}
