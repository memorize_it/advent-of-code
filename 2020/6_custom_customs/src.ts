import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

export async function parseInput(path: string): Promise<string[][]> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: string[][] = [];

      let i = 0;

      readInterface.on("line", (line) => {
        if (line === "") {
          i = i + 1;
          return;
        }

        if (!result[i]) {
          result[i] = [];
        }

        result[i].push(line);
      });

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

export function keepDifferents(string: string[]): string {
  let result = string[0];

  for (let i = 1; i < string.length; i++) {
    const currentString = string[i];

    for (let k = 0; k < currentString.length; k++) {
      const currentChar = currentString[k];
      if (!result.includes(currentChar)) {
        result = result.concat(currentChar);
      }
    }
  }

  return result;
}

export function calculateSum(
  answers: string[][],
  modification: (s: string[]) => string
) {
  return answers
    .map((a) => modification(a).length)
    .reduce((sum, a) => sum + a, 0);
}

export function keepMatches(string: string[]): string {
  let result = string[0];

  for (let i = 1; i < string.length; i++) {
    const currentString = string[i];
    result = matchingParts(result, currentString);
    if (result === "") {
      return "";
    }
  }

  return result;
}

export function matchingParts(s1: string, s2: string): string {
  for (let i = 0; i < s1.length; i++) {
    const char = s1[i];

    if (!s2.includes(char)) {
      s1 = s1.replace(char, "");
      i--;
    }

    if (s1 === "") {
      return s1;
    }
  }

  return s1;
}

export function part1(input: string[][]) {
  return calculateSum(input, keepDifferents);
}

export function part2(input: string[][]) {
  return calculateSum(input, keepMatches);
}
