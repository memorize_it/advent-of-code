import { parseInput, part1, part2 } from "./src";

xdescribe("6 - Custom Customs", () => {
  const inputFile = "./2020/6_custom_customs/input.txt";
  const testFile = "./2020/6_custom_customs/test.input.txt";

  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 11", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(11);
      });
    });

    describe("Input file", () => {
      it("Should return 6565", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(6565);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 6", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(6);
      });
    });

    describe("Input file", () => {
      it("Should return 3137", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(3137);
      });
    });
  });
});
