import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/10_adapter_array/input.txt";
const testFile = "./2020/10_adapter_array/test.input.txt";
const testFile2 = "./2020/10_adapter_array/test2.input.txt";

xdescribe("10 - Adapter Array", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 220", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(220);
      });
    });

    describe("Input file", () => {
      it("Should return 2775", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(2775);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 19208", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(19208);
      });
    });

    describe("Test file 2", () => {
      it("Should return 8", async () => {
        const input = await parseInput(testFile2);
        expect(part2(input)).toEqual(8);
      });
    });

    describe("Input file", () => {
      it("Should return 518344341716992", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(518344341716992);
      });
    });
  });
});
