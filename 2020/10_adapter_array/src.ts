import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

type InputType = number[];

export async function parseInput(path: string): Promise<InputType> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: number[] = [];
      readInterface.on("line", (line) => result.push(Number.parseInt(line)));

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

export function part1(input: InputType) {
  input.sort((a, b) => a - b);

  const deviceAdapter = input[input.length - 1] + 3;

  const { one, three } = [...input, deviceAdapter].reduce(
    (c, j, i) => {
      const previousIndex = i - 1;
      const previous = previousIndex < 0 ? 0 : input[previousIndex];
      const diff = j - previous;
      if (diff === 1) {
        return {
          ...c,
          one: c.one + 1,
        };
      }
      if (diff === 3) {
        return {
          ...c,
          three: c.three + 1,
        };
      }
      return c;
    },
    { one: 0, three: 0 }
  );

  return one * three;
}

function ascending(a: number, b: number) {
  return a - b;
}

export function part2(input: InputType) {
  return input
    .sort(ascending)
    .reduce(
      (a: number[], b: number) => {
        a[b] = (a[b - 3] || 0) + (a[b - 2] || 0) + (a[b - 1] || 0);
        return a;
      },
      [1]
    )
    .pop();
}
