import {
  convertToBinary,
  rowAndColumnToId,
  binaryStringToRowAndColumns,
  parseInput,
  searchMaxPass,
  getBoardingPassId,
} from "./src";

xdescribe("5 - Binary Boarding", () => {
  const inputFile = "./2020/5_binary_boarding/input.txt";
  const testFile = "./2020/5_binary_boarding/test.input.txt";

  describe("Part 1", () => {
    const testInput = ["BFFFBBFRRR", "FFFBBBFRRR", "BBFFBBFRLL"];

    describe("Test file", () => {
      it("Must parse read the result", async () => {
        const input = await parseInput(testFile);
        expect(input).toEqual(testInput);
      });

      it("Should return 820", () => {
        expect(searchMaxPass(testInput)).toEqual(820)
      });
    });

    describe("Test Column and Row to Id", () => {
      it("Should return 357", () => {
        expect(rowAndColumnToId(44, 5)).toEqual(357);
      });
    });

    describe("Convert string to binary", () => {
      it("Should return 0101100101", () => {
        expect(convertToBinary("FBFBBFFRLR")).toEqual("0101100101");
      });
    });

    describe("Convert binary string to row and columns", () => {
      it("Should return 0101100101", () => {
        expect(binaryStringToRowAndColumns("0101100101")).toEqual([44, 5]);
      });
    });

    describe("Input file", () => {
      it("Should return 861", async () => {
        const input = await parseInput(inputFile);
        expect(searchMaxPass(input)).toEqual(861)
      });
    });
  });

  describe("Part 2", () => {
    describe("Input file", () => {
      it("Should return 633", async () => {
        const input = await parseInput(inputFile);
        expect(getBoardingPassId(input)).toEqual(633)
      });
    });
  });
});
