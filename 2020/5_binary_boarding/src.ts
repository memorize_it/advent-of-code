import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

export function rowAndColumnToId(row: number, column: number) {
  return 8 * row + column;
}

export function convertToBinary(pass: string): string {
  return pass
    .split("")
    .map((current) => (current === "F" || current === "L" ? 0 : 1))
    .join("");
}

export function binaryStringToRowAndColumns(pass: string) {
  return [parseInt(pass.slice(0, 7), 2), parseInt(pass.slice(7, 10), 2)];
}

export function passToId(pass: string) {
  const [row, column] = binaryStringToRowAndColumns(convertToBinary(pass));
  return rowAndColumnToId(row, column);
}

export async function parseInput(path: string): Promise<string[]> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: string[] = [];
      readInterface.on("line", (line) => result.push(line));
      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

export function searchMaxPass(boardingPasses: string[]) {
  return boardingPasses
    .map(passToId)
    .reduce((max, n) => (n > max ? n : max), 0);
}

export function getBoardingPassId(boardingPasses: string[]) {
  const sortedPasses = boardingPasses.map((p) => passToId(p)).sort();

  for (
    let id = sortedPasses[0];
    id < sortedPasses[sortedPasses.length - 1];
    id++
  ) {
    if (!sortedPasses.includes(id)) {
      return id;
    }
  }
}
