import { ascending } from "../../utils/array";
import { parseInputAsArray } from "../../utils/input";

type MultidimensionalGrid = Map<string, boolean>;

type Coordinate = {
  xCol: number;
  yRow: number;
  zDim: number;
  wDim: number;
};

function coordinateToKey({ xCol, yRow, zDim, wDim }: Coordinate): string {
  return [xCol, yRow, zDim, wDim].join(",");
}

function keyToCoordinate(key: string): Coordinate {
  const [xCol, yRow, zDim, wDim] = key
    .split(",")
    .map((n) => Number.parseInt(n));
  return {
    xCol,
    yRow,
    zDim,
    wDim,
  };
}

export async function parseInput(path: string): Promise<MultidimensionalGrid> {
  const input = (await parseInputAsArray(path)).map((n) => n.split(""));
  const grid: MultidimensionalGrid = new Map();
  input.forEach((rowValue, yRow) => {
    rowValue.forEach((colValue, xCol) => {
      if (colValue === "#") {
        grid.set(coordinateToKey({ xCol, yRow, zDim: 0, wDim: 0 }), true);
      }
    });
  });

  return grid;
}

export function getNeighbours({
  xCol,
  yRow,
  zDim,
  wDim,
}: Coordinate): Coordinate[] {
  const neighbours: Coordinate[] = [];

  for (let i = -1; i <= 1; i++) {
    for (let j = -1; j <= 1; j++) {
      for (let k = -1; k <= 1; k++) {
        for (let l = -1; l <= 1; l++) {
          if (i !== 0 || j !== 0 || k !== 0 || l !== 0) {
            neighbours.push({
              xCol: xCol + i,
              yRow: yRow + j,
              zDim: zDim + k,
              wDim: wDim + l,
            });
          }
        }
      }
    }
  }

  return neighbours;
}

export function part1(input: MultidimensionalGrid) {
  const {
    minZDimension,
    maxZDimension,
    minYRow,
    maxYRow,
    minXCol,
    maxXCol,
  } = getMinAndMax(input);

  for (let i = 1; i < 7; i++) {
    const newVariants: MultidimensionalGrid = new Map();
    for (let zDim = minZDimension - i; zDim <= maxZDimension + i; zDim++) {
      for (let yRow = minYRow - i; yRow <= maxYRow + i; yRow++) {
        for (let xCol = minXCol - i; xCol <= maxXCol + i; xCol++) {
          const currentCube = { xCol, yRow, zDim, wDim: 0 };
          if (willActivate(currentCube, input)) {
            newVariants.set(coordinateToKey(currentCube), true);
          }
        }
      }
    }

    input = newVariants;
  }

  return countCells(input);
}

function willActivate(c: Coordinate, variants: MultidimensionalGrid): boolean {
  let cubeIsActive = variants.has(coordinateToKey(c));
  const activeNeighbours = getNeighbours(c).filter((coord) =>
    variants.has(coordinateToKey(coord))
  ).length;

  return cubeIsActive
    ? activeNeighbours === 2 || activeNeighbours === 3
    : activeNeighbours === 3;
}

function countCells(grid: MultidimensionalGrid): number {
  let count = 0;
  grid.forEach((row) => {
    if (row) {
      count += 1;
    }
  });
  return count;
}

function getMinAndMax(multiDimensionalGrid: MultidimensionalGrid) {
  const cols: number[] = [];
  const rows: number[] = [];
  const zDimensions: number[] = [];
  const wDimensions: number[] = [];

  multiDimensionalGrid.forEach((_, key) => {
    const { xCol, yRow, zDim, wDim } = keyToCoordinate(key);
    cols.push(xCol);
    rows.push(yRow);
    zDimensions.push(zDim);
    wDimensions.push(wDim);
  });

  cols.sort(ascending);
  rows.sort(ascending);
  zDimensions.sort(ascending);
  wDimensions.sort(ascending);

  return {
    minWDimension: wDimensions[0],
    maxWDimension: wDimensions[wDimensions.length - 1],
    minZDimension: zDimensions[0],
    maxZDimension: zDimensions[zDimensions.length - 1],
    minYRow: rows[0],
    maxYRow: rows[rows.length - 1],
    minXCol: cols[0],
    maxXCol: cols[cols.length - 1],
  };
}

export function part2(input: MultidimensionalGrid) {
  const {
    minWDimension,
    maxWDimension,
    minZDimension,
    maxZDimension,
    minYRow,
    maxYRow,
    minXCol,
    maxXCol,
  } = getMinAndMax(input);

  for (let i = 1; i < 7; i++) {
    const newVariants: MultidimensionalGrid = new Map();
    for (let wDim = minWDimension - i; wDim <= maxWDimension + i; wDim++) {
      for (let zDim = minZDimension - i; zDim <= maxZDimension + i; zDim++) {
        for (let yRow = minYRow - i; yRow <= maxYRow + i; yRow++) {
          for (let xCol = minXCol - i; xCol <= maxXCol + i; xCol++) {
            const currentCube = { xCol, yRow, zDim, wDim };
            if (willActivate(currentCube, input)) {
              newVariants.set(coordinateToKey(currentCube), true);
            }
          }
        }
      }
    }

    input = newVariants;
  }

  return countCells(input);
}
