import { getNeighbours, parseInput, part1, part2 } from "./src";

const inputFile = "./2020/17_conway_cubes/input.txt";
const testFile = "./2020/17_conway_cubes/test.input.txt";

xdescribe("17 - Conway Cubes", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 112", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(112);
      });
    });

    describe("Input file", () => {
      it("Should return 284", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(284);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 848", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(848);
      });
    });

    describe("Input file", () => {
      it("Should return 2240", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(2240);
      });
    });
  });
});
