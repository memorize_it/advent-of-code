type InputType = string;

function stringToNumberArray(s: string) {
  return s.split("").map((n) => Number.parseInt(n));
}

interface Node {
  previous: number;
  next: number;
}

class Circular {
  private _map: Map<number, Node>;

  constructor(arr: number[]) {
    this.initialize(arr);
  }

  private initialize(arr: number[]) {
    this._map = new Map();

    const nodesLength = arr.length;
    arr.forEach((v, i) => {
      this._map.set(v, {
        previous: arr[i - 1] || arr[nodesLength - 1],
        next: arr[(i + 1) % nodesLength],
      });
    });
  }

  private _forNextXElements(
    n: number,
    amount: number,
    cb: (n: Node, value: number, map: Map<number, Node>) => void
  ) {
    for (let i = 0; i < amount; i++) {
      const element = this._map.get(n);
      cb(element, n, this._map);
      n = element.next;
    }
  }

  public getNextElement(n: number): number {
    return this._map.get(n).next;
  }

  public getNextXElements(n: number, amount: number): number[] {
    let result: number[] = [];

    this._forNextXElements(n, amount, (element) => {
      result.push(element.next);
    });

    return result;
  }

  public includes(n: number) {
    return this._map.has(n);
  }

  public removeNextXElement(n: number, amount: number): number[] {
    const currentElement = this._map.get(n);

    let lastNext: number;
    const deletedElements: number[] = [];

    this._forNextXElements(
      currentElement.next,
      amount,
      (deletedElement, v, map) => {
        map.delete(v);
        deletedElements.push(v);
        lastNext = deletedElement.next;
      }
    );

    currentElement.next = lastNext;
    this._map.get(lastNext).previous = n;
    
    return deletedElements;
  }

  public removeElement(n: number) {
    const { next, previous } = this._map.get(n);
    this._map.get(previous).next = next;
    this._map.get(next).previous = previous;
    this._map.delete(n);
  }

  public insertAfterElement(n: number, numbers: number[]) {
    let currentElement = this._map.get(n);
    let lastElement = currentElement.next;

    currentElement.next = numbers[0];
    numbers.forEach((v, i) => {
      this._map.set(v, {
        previous: numbers[i - 1] || n,
        next: numbers[i + 1] || lastElement,
      });
      n = v;
    });
  }

  public toString(n: number, separator = "", printFirst = true): string {
    let element = this._map.get(n);
    const array: string[] = printFirst ? [`(${n})`] : [];
    while (element.next !== n) {
      array.push(`${element.next}`);
      element = this._map.get(element.next);
    }

    return array.join(separator);
  }
}

export function part1(input: InputType, turns: number = 100) {
  const initialArray = stringToNumberArray(input);
  let cups: Circular = new Circular(initialArray);
  const lowestCup = 1;
  const highestCup = 9;
  let currentCup = initialArray[0];

  playGame(currentCup, turns, cups, lowestCup, highestCup);

  return cups.toString(1, "", false);
}

function playGame(
  currentCup: number,
  turns: number,
  cups: Circular,
  lowestCup: number,
  highestCup: number
) {
  for (let turn = 0; turn < turns; turn++) {
    const nextCups = cups.removeNextXElement(currentCup, 3);
    const nextCup = cups.getNextElement(currentCup);

    let found = false;
    let destinationCup = currentCup - 1;
    while (!found) {
      if (cups.includes(destinationCup)) {
        found = true;
      } else {
        destinationCup--;
        if (destinationCup < lowestCup) {
          destinationCup = highestCup;
        }
      }
    }

    cups.insertAfterElement(destinationCup, nextCups);
    currentCup = nextCup;
  }
}

export function part2(input: InputType, turns: number = 10000000) {
  const lowestCup = 1;
  const highestCup = 1000000;
  const initalArray = stringToNumberArray(input);

  for (let n = 10; n <= 1000000; n++) {
    initalArray.push(n);
  }

  let cups = new Circular(initalArray);
  let currentCup = initalArray[0];

  playGame(currentCup, turns, cups, lowestCup, highestCup);

  return cups.getNextXElements(1, 2).reduce((c, i) => c * i, 1);
}
