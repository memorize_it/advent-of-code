import { part1, part2 } from "./src";

xdescribe("23 - Crab Cup", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 67384529", async () => {
        expect(part1("389125467")).toEqual("67384529");
      });
    });

    describe("Input file", () => {
      it("Should return 25368479", async () => {
        expect(part1("326519478")).toEqual("25368479");
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 149245887792", async () => {
        expect(part2("389125467")).toEqual(149245887792);
      });
    });

    describe("Input file", () => {
      it("Should return 44541319250", async () => {
        expect(part2("326519478")).toEqual(44541319250);
      });
    });
  });
});
