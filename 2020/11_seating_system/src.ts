import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

type InputType = string[][];

export async function parseInput(path: string): Promise<InputType> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: string[][] = [];
      readInterface.on("line", (line) => {
        const row: string[] = line.split("");
        result.push(row);
      });

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

function getAdjacentSeatsInRow(
  row: string[],
  col: number,
  include = true
): number {
  let adjacentSeats = 0;

  if (col !== 0 && row[col - 1] === "#") {
    adjacentSeats++;
  }

  if (include && row[col] === "#") {
    adjacentSeats++;
  }

  if (col < row.length - 1 && row[col + 1] === "#") {
    adjacentSeats++;
  }

  return adjacentSeats;
}

function getAdjacentSeats(input: InputType, row: number, col: number): number {
  let adjacentSeats = getAdjacentSeatsInRow(input[row], col, false);

  if (row !== 0) {
    adjacentSeats = adjacentSeats + getAdjacentSeatsInRow(input[row - 1], col);
  }

  if (row < input.length - 1) {
    adjacentSeats = adjacentSeats + getAdjacentSeatsInRow(input[row + 1], col);
  }

  return adjacentSeats;
}

export function part1(input: InputType) {
  let stateChange = true;
  while (stateChange) {
    const originalInput: InputType = input.map((r) => [...r]);
    stateChange = false;

    for (let r = 0; r < input.length; r++) {
      const row = input[r];
      for (let c = 0; c < row.length; c++) {
        const seat = row[c];
        const adjacentSeats = getAdjacentSeats(originalInput, r, c);

        if (seat === "L" && adjacentSeats <= 0) {
          row[c] = "#";
          stateChange = true;
        }

        if (seat === "#" && adjacentSeats >= 4) {
          row[c] = "L";
          stateChange = true;
        }
      }
    }
  }

  return input
    .map((r) => r.reduce((c, s) => (s === "#" ? c + 1 : c), 0))
    .reduce((total, c) => total + c, 0);
}

function getOccupiedAdjacentSeatsInSection(
  input: InputType,
  startRowNumber: number,
  startColNumber: number,
  isCurrentRow = true,
  moveDown = false
): number {
  let adjacentSeats = 0;
  const maxCol = input[0].length - 1;
  const maxRow = input.length - 1;

  let leftFound = false;
  let rightFound = false;
  let middleFound = isCurrentRow;

  for (
    let i = 0, leftCol = startColNumber - 1, rightCol = startColNumber + 1;
    !leftFound || (!isCurrentRow && !middleFound) || !rightFound;
    i++, leftCol--, rightCol++
  ) {
    const currentRow = isCurrentRow
      ? startRowNumber
      : moveDown
      ? startRowNumber + i
      : startRowNumber - i;

    const tooFarDown = currentRow > maxRow;
    const tooFarUp = currentRow < 0;
    const tooFarLeft = leftCol < 0;
    const tooFarRight = rightCol > maxCol;
    if (tooFarDown || tooFarUp || (tooFarLeft && tooFarRight)) {
      break;
    }

    const row = input[currentRow];

    if (!tooFarLeft && !leftFound) {
      const s = row[leftCol];

      if (s !== ".") {
        leftFound = true;

        if (s === "#") {
          adjacentSeats++;
        }
      }
    }

    if (!middleFound) {
      const s = row[startColNumber];

      if (s !== ".") {
        middleFound = true;

        if (s === "#") {
          adjacentSeats++;
        }
      }
    }

    if (!tooFarRight && !rightFound) {
      const s = row[rightCol];

      if (s !== ".") {
        rightFound = true;

        if (s === "#") {
          adjacentSeats++;
        }
      }
    }
  }

  return adjacentSeats;
}

function getOccupiedAdjacentSeats(
  input: InputType,
  row: number,
  col: number
): number {
  let adjacentSeats = getOccupiedAdjacentSeatsInSection(input, row, col);

  if (row !== 0) {
    adjacentSeats =
      adjacentSeats +
      getOccupiedAdjacentSeatsInSection(input, row - 1, col, false);
  }

  if (row < input.length - 1) {
    adjacentSeats =
      adjacentSeats +
      getOccupiedAdjacentSeatsInSection(input, row + 1, col, false, true);
  }

  return adjacentSeats;
}

export function part2(input: InputType) {
  let stateChange = true;
  let i = 1;

  while (stateChange) {
    const originalInput: InputType = input.map((r) => [...r]);
    stateChange = false;

    for (let r = 0; r < input.length; r++) {
      const row = input[r];
      for (let c = 0; c < row.length; c++) {
        const seat = row[c];
        const adjacentSeats = getOccupiedAdjacentSeats(originalInput, r, c);

        if (seat === "L" && adjacentSeats === 0) {
          row[c] = "#";
          stateChange = true;
        }

        if (seat === "#" && adjacentSeats >= 5) {
          row[c] = "L";
          stateChange = true;
        }
      }
    }

    i++;
  }

  return input
    .map((r) => r.reduce((c, s) => (s === "#" ? c + 1 : c), 0))
    .reduce((total, c) => total + c, 0);
}
