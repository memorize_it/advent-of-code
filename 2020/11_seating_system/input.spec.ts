import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/11_seating_system/input.txt";
const testFile = "./2020/11_seating_system/test.input.txt";
const test2File = "./2020/11_seating_system/test2.input.txt";

xdescribe("11 : Seating System", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 37", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(37);
      });
    });

    describe("Input file", () => {
      it("Should return 2476", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(2476);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 26", async () => {
        const input = await parseInput(test2File);
        expect(part2(input)).toEqual(26);
      });
    });

    describe("Input file", () => {
      it("Should return 2257", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(2257);
      });
    });
  });
});
