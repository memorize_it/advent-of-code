import { parseInputAsArray } from "../../utils/input";

type InputType = string[];

type TileMap = Map<string, boolean>;

interface HexTile {
  row: number;
  column: number;
}

const DirectionsMap: Map<string, HexTile> = new Map();

DirectionsMap.set("e", {
  row: 0,
  column: 2,
});

DirectionsMap.set("se", {
  row: -1,
  column: 1,
});

DirectionsMap.set("sw", {
  row: -1,
  column: -1,
});

DirectionsMap.set("w", {
  row: 0,
  column: -2,
});

DirectionsMap.set("nw", {
  row: 1,
  column: -1,
});

DirectionsMap.set("ne", {
  row: 1,
  column: 1,
});

function tileToIdentifier({ row, column }: HexTile): string {
  return `${row},${column}`;
}

function identifierToTile(id: string): HexTile {
  const [row, column] = id.split(",");
  return { row: Number.parseInt(row), column: Number.parseInt(column) };
}

function getAdjacentTiles(tile: HexTile): string[] {
  const result = [];

  DirectionsMap.forEach((c) => {
    result.push(tileToIdentifier(combineTiles(c, tile)));
  });

  return result;
}

function move(p: HexTile, path: string) {
  if (!path.length) {
    return p;
  }

  let direction = path[0];
  let next: string;

  if (direction === "e" || direction === "w") {
    next = path.slice(1, path.length);
  } else {
    direction += path[1];
    next = path.slice(2, path.length);
  }

  const dir = DirectionsMap.get(direction);
  return move(combineTiles(p, dir), next);
}

function combineTiles(p: HexTile, dir: HexTile): HexTile {
  return {
    row: p.row + dir.row,
    column: p.column + dir.column,
  };
}

export async function parseInput(path: string): Promise<InputType> {
  return parseInputAsArray(path);
}

export function part1(input: InputType) {
  const tileMap = getTileMap(input);
  return countBlackTiles(tileMap);
}

function countBlackTiles(tileMap: TileMap) {
  let c = 0;

  tileMap.forEach((isBlack) => {
    if (isBlack) {
      c++;
    }
  });

  return c;
}

function getTileMap(input: InputType): TileMap {
  return input
    .map((i) => tileToIdentifier(move({ column: 0, row: 0 }, i)))
    .reduce((c: TileMap, i) => {
      c.set(i, c.has(i) ? !c.get(i) : true);
      return c;
    }, new Map());
}

function blackTileMapToAllTileMap(map: TileMap) {
  let maxCol: number;
  let maxRow: number;
  let minCol: number;
  let minRow: number;

  map.forEach((isBlack, id) => {
    if (!isBlack) {
      return;
    }
    const { column, row } = identifierToTile(id);
    maxCol = maxCol ? (column > maxCol ? column : maxCol) : column;
    minCol = minCol ? (column < minCol ? column : minCol) : column;
    maxRow = maxRow ? (row > maxRow ? row : maxRow) : row;
    minRow = minRow ? (row < minRow ? row : minRow) : row;
  });

  const newMap: TileMap = new Map();
  for (let row = minRow - 1; row <= maxRow + 1; row++) {
    for (let column = minCol - 1; column <= maxCol + 1; column++) {
      const id = tileToIdentifier({ row, column });
      if (!map.has(id)) {
        newMap.set(id, false);
      } else {
        newMap.set(id, map.get(id));
      }
    }
  }

  return newMap;
}

export function part2(input: InputType, maxTours: number = 100) {
  let tileMap: TileMap = getTileMap(input);

  for (let i = 1; i <= maxTours; i++) {
    const completeTileMap = blackTileMapToAllTileMap(tileMap);
    const newTileMap = new Map();

    completeTileMap.forEach((isBlack, tileId) => {
      const adjacentTiles = getAdjacentTiles(identifierToTile(tileId))
        .map((tileId) => completeTileMap.get(tileId))
        .filter(Boolean).length;

      if (isBlack) {
        if (adjacentTiles > 0 && adjacentTiles <= 2) {
          newTileMap.set(tileId, true);
        }
        return;
      }
      
      if (adjacentTiles === 2) {
        newTileMap.set(tileId, true);
      }
    });

    tileMap = newTileMap;
  }

  return countBlackTiles(tileMap);
}
