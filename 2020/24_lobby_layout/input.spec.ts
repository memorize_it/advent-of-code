import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/24_lobby_layout/input.txt";
const testFile = "./2020/24_lobby_layout/test.input.txt";

xdescribe("24 - Lobby Layout", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 10", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(10);
      });
    });

    describe("Input file", () => {
      it("Should return 500", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(500);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 2208", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(2208);
      });
    });

    describe("Input file", () => {
      it("Should return 4280", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(4280);
      });
    });
  });
});
