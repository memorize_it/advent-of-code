import { parseInputAsArray } from "../../utils/input";

type InputType = {
  ingredients: string[];
  allergens: string[];
}[];

export async function parseInput(path: string): Promise<InputType> {
  const input = await parseInputAsArray(path);

  return input.map((l) => {
    const [ingredients, allergens] = l.split(" (contains ");
    return {
      ingredients: ingredients.split(" "),
      allergens: allergens
        ? allergens.slice(0, allergens.length - 1).split(", ")
        : [],
    };
  });
}

export function part1(input: InputType) {
  const {
    allergensWithPossibleIngredients,
    ingredientCount,
  }: {
    allergensWithPossibleIngredients: Map<string, string[]>;
    ingredientCount: Map<string, number>;
  } = parseAllergens(input);

  allergensWithPossibleIngredients.forEach((mayContainAllergen) => {
    mayContainAllergen.forEach((i) => {
      if (ingredientCount.has(i)) {
        ingredientCount.delete(i);
      }
    });
  });

  let count = 0;
  ingredientCount.forEach((value) => {
    count += value;
  });

  return count;
}

function parseAllergens(input: InputType) {
  const allergensWithPossibleIngredients: Map<string, string[]> = new Map();
  const ingredientCount: Map<string, number> = new Map();
  input.forEach(({ ingredients, allergens }) => {
    allergens.forEach((a) => {
      if (allergensWithPossibleIngredients.has(a)) {
        const possibleIngredients = allergensWithPossibleIngredients.get(a);
        allergensWithPossibleIngredients.set(
          a,
          possibleIngredients.filter((i) => ingredients.includes(i))
        );
      } else {
        allergensWithPossibleIngredients.set(a, ingredients);
      }
    });
    ingredients.forEach((i) => {
      if (ingredientCount.has(i)) {
        ingredientCount.set(i, ingredientCount.get(i) + 1);
      } else {
        ingredientCount.set(i, 1);
      }
    });
  });
  return { allergensWithPossibleIngredients, ingredientCount };
}

export function part2(input: InputType) {
  const {
    allergensWithPossibleIngredients,
  }: {
    allergensWithPossibleIngredients: Map<string, string[]>;
    ingredientCount: Map<string, number>;
  } = parseAllergens(input);

  let processFinished = false;
  while (!processFinished) {
    allergensWithPossibleIngredients.forEach((possibleIngredients1, key1) => {
      if (possibleIngredients1.length === 1) {
        const assignedIngredient = possibleIngredients1[0];
        allergensWithPossibleIngredients.forEach(
          (possibleIngredients2, key2) => {
            if (key1 !== key2) {
              const ingredientIndex = possibleIngredients2.indexOf(
                assignedIngredient
              );
              if (ingredientIndex >= 0) {
                allergensWithPossibleIngredients.set(key2, [
                  ...possibleIngredients2.slice(0, ingredientIndex),
                  ...possibleIngredients2.slice(
                    ingredientIndex + 1,
                    possibleIngredients2.length
                  ),
                ]);
              }
            }
          }
        );
      }
    });

    processFinished = true;
    allergensWithPossibleIngredients.forEach((value) => {
      if (value.length > 1) {
        processFinished = false;
      }
    });
  }

  const values: [string, string][] = [];
  allergensWithPossibleIngredients.forEach((ingredients, allergen) => {
    values.push([allergen, ingredients[0]]);
  });

  return values
    .sort(([a], [b]) => (a < b ? -1 : 1))
    .map(([_, i]) => i)
    .join(",");
}
