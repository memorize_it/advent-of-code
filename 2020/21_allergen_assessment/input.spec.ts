import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/21_allergen_assessment/input.txt";
const testFile = "./2020/21_allergen_assessment/test.input.txt";

xdescribe("21 - Allergen Assessment", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 5", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(5);
      });
    });

    describe("Input file", () => {
      it("Should return 2428", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(2428);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 'mxmxvkd,sqjhc,fvjkl'", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual("mxmxvkd,sqjhc,fvjkl");
      });
    });

    describe("Input file", () => {
      it("Should return 67587168", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(67587168);
      });
    });
  });
});
