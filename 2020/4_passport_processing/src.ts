import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

const validKeysWithValidation = {
  byr: (v: string) => 1920 <= Number.parseInt(v) && Number.parseInt(v) <= 2002,
  iyr: (v: string) => 2010 <= Number.parseInt(v) && Number.parseInt(v) <= 2020,
  eyr: (v: string) => 2020 <= Number.parseInt(v) && Number.parseInt(v) <= 2030,
  hgt: (v: string) => {
    if (v.includes("cm")) {
      const [cm] = v.split("cm");
      const cms = Number.parseInt(cm);
      return 150 <= cms && cms <= 193;
    }

    if (v.includes("in")) {
      const [inches] = v.split("in");
      const ins = Number.parseInt(inches);
      return 59 <= ins && ins <= 76;
    }

    return false;
  },
  hcl: (v: string) => v.match(/^#[0-9a-f]{6}$/),
  ecl: (v: string) =>
    ["amb", "blu", "brn", "gry", "grn", "hzl", "oth"].includes(v),
  pid: (v: string) => v.match(/^[0-9]{9}$/),
  cid: (_: string) => true,
};
const validKeys = Object.getOwnPropertyNames(validKeysWithValidation).sort();

type Passport = { [key: string]: string };

export async function parseInput(path: string): Promise<Passport[]> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: Passport[] = [];

      let currentPassport = {};

      readInterface.on("line", (line) => {
        if (line === "") {
          result.push(currentPassport);
          currentPassport = {};
          return;
        }

        const values = line.split(" ");
        for (let i = 0; i < values.length; i++) {
          const [key, value] = values[i].split(":");
          currentPassport[key] = value;
        }
      });

      readInterface.on("close", () => {
        res(result);
      });
    } catch (error) {
      rej(error);
    }
  });
}

export function countValidPassports(p: Passport[], validate = false) {
  return p.filter((p) => {
    const amountOfValidKeys = validKeys.length;

    const objectProperties = Object.getOwnPropertyNames(p).sort();
    const amountOfKeysInPassport = objectProperties.length;

    if (
      amountOfKeysInPassport < amountOfValidKeys - 1 ||
      amountOfKeysInPassport > amountOfValidKeys
    ) {
      return false;
    }

    const missingKeys = validKeys.filter((p) => !objectProperties.includes(p));
    const amountOfMissingKeys = missingKeys.length;
    if (amountOfMissingKeys > 1) {
      return false;
    }

    const objectHasCid = !missingKeys.includes("cid");
    if (amountOfMissingKeys === 1 && objectHasCid) {
      return false;
    }

    if (!validate) {
      return true;
    }

    for (let i = 0; i < amountOfKeysInPassport; i++) {
      const key = objectProperties[i];
      const value = p[key];
      const validate = validKeysWithValidation[key];

      if (!validate(value)) {
        return false;
      }
    }

    return true;
  }).length;
}
