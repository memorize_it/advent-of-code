import { parseInput, countValidPassports } from "./src";

xdescribe("4 - Passport Processing", () => {
  const inputFile = "./2020/4_passport_processing/input.txt";
  const testFile = "./2020/4_passport_processing/test.input.txt";
  const invalidTestFile = "./2020/4_passport_processing/invalid.test.input.txt";
  const validTestFile = "./2020/4_passport_processing/valid.test.input.txt";

  const testInput = [
    {
      ecl: "gry",
      pid: "860033327",
      eyr: "2020",
      hcl: "#fffffd",
      byr: "1937",
      iyr: "2017",
      cid: "147",
      hgt: "183cm",
    },
    {
      byr: "1929",
      cid: "350",
      ecl: "amb",
      eyr: "2023",
      hcl: "#cfa07d",
      iyr: "2013",
      pid: "028048884",
    },
    {
      hcl: "#ae17e1",
      iyr: "2013",
      eyr: "2024",
      ecl: "brn",
      pid: "760753108",
      byr: "1931",
      hgt: "179cm",
    },
    {
      hcl: "#cfa07d",
      eyr: "2025",
      pid: "166559648",
      iyr: "2011",
      ecl: "brn",
      hgt: "59in",
    },
  ];

  describe("Part 1", () => {
    describe("Test file", () => {
      it("Must parse read the result", async () => {
        const testInputResult = await parseInput(testFile);
        expect(testInputResult).toEqual(testInput);
      });

      it("Must return 2", () => {
        expect(countValidPassports(testInput)).toEqual(2);
      });
    });

    describe("Input file", () => {
      it("Should return 206", async () => {
        const input = await parseInput(inputFile);
        expect(countValidPassports(input)).toEqual(206);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Must return 0", async () => {
        const input = await parseInput(invalidTestFile);
        expect(countValidPassports(input, true)).toEqual(0);
      });

      it("Must return 4", async () => {
        const input = await parseInput(validTestFile);
        expect(countValidPassports(input, true)).toEqual(4);
      });
    });

    describe("Input file", () => {
      it("Should return 123", async () => {
        const input = await parseInput(inputFile);
        expect(countValidPassports(input, true)).toEqual(123);
      });
    });
  });
});
