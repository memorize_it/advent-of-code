import { parseInputAsArray } from "../../utils/input";

type Ticket = number[];
type Range = [number, number];
type Rules = Map<string, [Range, Range]>;
type InputType = {
  rules: Rules;
  yourTicket: Ticket;
  nearbyTickets: Ticket[];
};

export async function parseInput(path: string): Promise<InputType> {
  const parsedInput = await parseInputAsArray(path);

  const rules: Rules = new Map();
  let yourTicket = [];
  let nearbyTickets = [];

  let rulesParsed = false;
  let ticketParsed = false;

  parsedInput.forEach((l) => {
    if (l === "") {
      return;
    }

    if (l === "your ticket:") {
      rulesParsed = true;
      return;
    }

    if (l === "nearby tickets:") {
      ticketParsed = true;
      return;
    }

    if (!rulesParsed) {
      const [rule, values] = l.split(": ");
      const ruleRangeStrings = values.split(" or ");
      if (ruleRangeStrings.length < 2) {
        throw new Error("Invalid rule");
      }

      const ruleRanges = ruleRangeStrings.map(
        (r): Range => {
          const range = r.split("-").map((c) => Number.parseInt(c));
          if (range.length < 2) {
            throw new Error("Invalid range");
          }

          return [range[0], range[1]];
        }
      );

      rules.set(rule, [ruleRanges[0], ruleRanges[1]]);
      return;
    }

    if (!ticketParsed) {
      yourTicket = l.split(",").map((c) => Number.parseInt(c));
      return;
    }

    nearbyTickets.push(l.split(",").map((c) => Number.parseInt(c)));
  });

  return {
    rules,
    yourTicket,
    nearbyTickets,
  };
}

function inRange(value: number, simplifiedRules: Range[]) {
  for (let i = 0; i < simplifiedRules.length; i++) {
    const [min, max] = simplifiedRules[i];
    if (value >= min && value <= max) {
      return true;
    }
  }
  return false;
}

export function part1(input: InputType) {
  const rules: Range[] = [];
  input.rules.forEach((v) => rules.push(...v));

  return input.nearbyTickets.reduce(
    (c, r) =>
      c +
      r.filter((v) => !inRange(v, rules)).reduce((rangeC, v) => rangeC + v, 0),
    0
  );
}

function getFilteredSeats({ rules, nearbyTickets }: InputType) {
  const rulesArray: Range[] = [];
  rules.forEach((rule) => rulesArray.push(...rule));
  return nearbyTickets.filter((ticket) =>
    ticket.every((v) => inRange(v, rulesArray))
  );
}

export function part2(input: InputType) {
  const { yourTicket, rules } = input;

  const validTickets: Ticket[] = [yourTicket, ...getFilteredSeats(input)];
  const mapIndexes: Map<number, number[]> = new Map();

  for (let i = 0; i < yourTicket.length; i++) {
    mapIndexes.set(
      i,
      validTickets.map((values) => values[i])
    );
  }

  const orderedRules: Map<string, number[]> = new Map();
  rules.forEach((_, ruleName) => orderedRules.set(ruleName, []));

  for (let i = 0; i < yourTicket.length; i++) {
    rules.forEach((ranges, ruleName) => {
      const allValuesInRange = mapIndexes
        .get(i)
        .every((n: number) => inRange(n, ranges));
      if (allValuesInRange) {
        orderedRules.get(ruleName).push(i);
      }
    });
  }

  const rulesWithPositions = Object.fromEntries(orderedRules);
  const allRuleNames = Object.getOwnPropertyNames(rulesWithPositions);

  for (let i = 0; i < allRuleNames.length; i++) {
    const keyWithSingleOption = allRuleNames
      .filter((k) => rulesWithPositions[k].length === 1)
      .reduce((c, r) => [...c, ...rulesWithPositions[r]], []);

    allRuleNames
      .filter((r) => rulesWithPositions[r].length > 1)
      .forEach((r) => {
        const currentIndexes = rulesWithPositions[r];
        rulesWithPositions[r] = [...currentIndexes].filter(
          (n) => !keyWithSingleOption.includes(n)
        );
      });
  }

  return allRuleNames.reduce(
    (c, name) =>
      name.includes("departure")
        ? c * yourTicket[rulesWithPositions[name][0]]
        : c,
    1
  );
}
