import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/16_ticket_translation/input.txt";
const testFile = "./2020/16_ticket_translation/test.input.txt";
const test2File = "./2020/16_ticket_translation/test2.input.txt";

xdescribe("16 - Ticket Translation", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 71", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(71);
      });
    });

    describe("Input file", () => {
      it("Should return 29019", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(29019);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 12", async () => {
        const input = await parseInput(test2File);
        expect(part2(input)).toEqual(12);
      });
    });

    describe("Input file", () => {
      it("Should return 517827547723", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(517827547723);
      });
    });
  });
});
