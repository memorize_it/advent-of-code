import { getMin, parseInput, part1, part2 } from "./src";

const inputFile = "./2020/13_shuttle_search/input.txt";
const testFile = "./2020/13_shuttle_search/test.input.txt";

xdescribe("13 - Shuttle Search", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 295", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(295);
      });
    });

    describe("Input file", () => {
      it("Should return 1915", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(1915);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test cases", () => {
      const tests = {
        3417: ["17", "x", "13", "19"],
        754018: ["67", "7", "59", "61"],
        779210: ["67", "x", "7", "59", "61"],
        1261476: ["67", "7", "x", "59", "61"],
        1202161486: ["1789", "37", "47", "1889"],
      };

      const casesArray = Object.getOwnPropertyNames(tests);

      for (let i = 0; i < casesArray.length; i++) {
        const c = casesArray[i];
        const input = tests[c];

        describe(`Should return ${c} for input ${input}`, () => {
          expect(getMin(input)).toEqual(Number.parseInt(c));
        });
      }
    });

    describe("Test file", () => {
      it("Should return 1068788", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(1068788);
      });
    });

    describe("Input file", () => {
      it("Should return 294354277694107", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(294354277694107);
      });
    });
  });
});
