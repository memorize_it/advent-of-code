import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

type InputType = {
  depart: number;
  buses: string[];
};

export async function parseInput(path: string): Promise<InputType> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: InputType = {
        depart: undefined,
        buses: [],
      };
      readInterface.on("line", (line) => {
        if (!result.depart) {
          result.depart = Number.parseInt(line);
          return;
        }

        result.buses = line.split(",");
      });

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

export function part1({ depart, buses }: InputType) {
  let filteredBuses = buses
    .filter((n) => n !== "x")
    .map((n) => Number.parseInt(n));
  const currentValues: number[] = [];

  for (let i = 0; i < buses.length; i++) {
    const base = filteredBuses[i];

    let result = base;
    let multiplier = 1;

    while (result < depart) {
      multiplier++;
      result = base * multiplier;
    }

    currentValues[i] = result;
  }

  const firstDepart = [...currentValues].sort((a, b) => a - b)[0];
  return (
    (firstDepart - depart) * filteredBuses[currentValues.indexOf(firstDepart)]
  );
}

function isValidDepart(timestamp: number, index: number, id: number) {
  return (timestamp + index) % id === 0;
}

export function getMin(input: string[]) {
  const buses = input
    .map((id, index) => ({
      id,
      index,
    }))
    .filter(({ id }) => id !== "x")
    .map(({ id, index }) => ({
      index,
      id: Number.parseInt(id),
    }));

  let increment = buses[0].id;
  let timestamp = increment;
  let nextBusIndex = 1;

  while (true) {
    const valid = buses.every(({ id, index }) =>
      isValidDepart(timestamp, index, id)
    );

    if (valid) {
      return timestamp;
    }

    const { id, index } = buses[nextBusIndex];

    if (isValidDepart(timestamp, index, id)) {
      increment *= id;
      nextBusIndex++;
    }

    timestamp += increment;
  }
}

export function part2(input: InputType) {
  return getMin(input.buses);
}
