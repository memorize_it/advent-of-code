import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

export interface PasswordInterface {
  range: {
    min: number;
    max: number;
  };
  char: string;
  password: string;
}

export async function parseInput(path: string): Promise<PasswordInterface[]> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: PasswordInterface[] = [];

      readInterface.on("line", (line) => {
        const parts = line.split(" ");
        const range = parts[0].split("-");

        result.push({
          range: {
            min: Number.parseInt(range[0]),
            max: Number.parseInt(range[1]),
          },
          char: parts[1][0],
          password: parts[2],
        });
      });

      readInterface.on("close", () => {
        res(result);
      });
    } catch (error) {
      rej(error);
    }
  });
}

export function countInString(str: string, stringsearch: string): number {
  let count = 0;
  for (var i = 0; i < str.length; i++) {
    if (stringsearch === str[i]) {
      count += 1;
    }
  }

  return count;
}

export function countCorrectPasswordsPart1(pwds: PasswordInterface[]): number {
  return countCorrectPasswords(
    pwds,
    ({ password, char, range: { min, max } }) => {
      const count = countInString(password, char);
      return count >= min && count <= max;
    }
  );
}

export function countCorrectPasswordsPart2(pwds: PasswordInterface[]): number {
  return countCorrectPasswords(
    pwds,
    ({ password, char, range: { min, max } }) =>
      (password[min - 1] === char) != (password[max - 1] === char)
  );
}

function countCorrectPasswords(
  pwds: PasswordInterface[],
  check: (t: PasswordInterface) => boolean
): number {
  return pwds.reduce((c, p) => (check(p) ? c + 1 : c), 0);
}
