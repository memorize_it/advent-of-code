import {
  countInString,
  countCorrectPasswordsPart1,
  parseInput,
  PasswordInterface,
  countCorrectPasswordsPart2,
} from "./src";

xdescribe("2 - Password Philosophy", () => {
  const testInput: PasswordInterface[] = [
    {
      range: {
        min: 1,
        max: 3,
      },
      char: "a",
      password: "abcde",
    },
    {
      range: {
        min: 1,
        max: 3,
      },
      char: "b",
      password: "cdefg",
    },
    {
      range: {
        min: 2,
        max: 9,
      },
      char: "c",
      password: "ccccccccc",
    },
  ];

  const inpuFile = "./2020/2_password_philosophy/part-1.input.txt";

  describe("Part 1", () => {
    describe("Test file", () => {
      it("Must parse read the result", async () => {
        const result = await parseInput(
          "./2020/2_password_philosophy/test.input.txt"
        );
        expect(result).toEqual(testInput);
      });

      it("Must count char in string", () => {
        expect(countInString("abcde", "a")).toEqual(1);
      });

      it("Must return 2", () => {
        expect(countCorrectPasswordsPart1(testInput)).toEqual(2);
      });
    });

    describe("Input file", () => {
      it("Must return 586", async () => {
        const input = await parseInput(inpuFile);
        expect(countCorrectPasswordsPart1(input)).toEqual(586);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Must return 2", () => {
        expect(countCorrectPasswordsPart2(testInput)).toEqual(1);
      });
    });

    describe("Input file", () => {
      it("Must return 352", async () => {
        const input = await parseInput(inpuFile);
        expect(countCorrectPasswordsPart2(input)).toEqual(352);
      });
    });
  });
});
