import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/7_handy_haversacks/input.txt";
const testFile = "./2020/7_handy_haversacks/test.input.txt";
const testFile2 = "./2020/7_handy_haversacks/test2.input.txt";

xdescribe("Day 7 : Handy Haversacks", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 4", async () => {
        const input = await parseInput(testFile);
        expect(part1(input, "shiny_gold", 1)).toEqual(4);
      });
    });

    describe("Input file", () => {
      it("Should return 179", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input, "shiny_gold", 1)).toEqual(179);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 126", async () => {
        const input = await parseInput(testFile2);
        expect(part2(input, "shiny_gold")).toEqual(126);
      });
    });

    describe("Input file", () => {
      it("Should return 18925", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input, "shiny_gold")).toEqual(18925);
      });
    });
  });
});
