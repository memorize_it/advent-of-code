import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

export type Bag = {
  [key: string]: number;
};
export type Rules = {
  [key: string]: Bag | undefined;
};

export async function parseInput(path: string): Promise<Rules> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const rules: Rules = {};

      readInterface.on("line", (line) => {
        const [name, containedBags] = line.split(" bags contain ");
        rules[name.split(" ").join("_")] =
          containedBags === "no other bags."
            ? undefined
            : containedBags
                .split(", ")
                .map((rule) => {
                  const [r] = rule.split(/\ bags?(\.?|\,\ )/);
                  const [amount, ...name] = r.split(" ");
                  return [amount, name.join("_")];
                })
                .reduce((p, [amount, name]) => {
                  p[name] = Number.parseInt(amount);
                  return p;
                }, {});
      });

      readInterface.on("close", () => res(rules));
    } catch (error) {
      rej(error);
    }
  });
}

export function parseRule(
  rules: Rules,
  parseKey: string,
  searchKey: string,
  amount: number
): boolean {
  const bag = rules[parseKey];
  if (!bag) {
    return false;
  }

  const bagCanContain = Object.getOwnPropertyNames(bag);
  if (!bagCanContain.length) {
    return false;
  }

  if (bagCanContain.includes(searchKey)) {
    return bag[searchKey] >= amount;
  }

  for (let i = 0; i < bagCanContain.length; i++) {
    if (parseRule(rules, bagCanContain[i], searchKey, amount)) {
      return true;
    }
  }
}

export function parseSumRule(rules: Rules, parseKey: string): number {
  const bag = rules[parseKey];
  if (!bag) {
    return 0;
  }

  const bagCanContain = Object.getOwnPropertyNames(bag);
  let sum = 0;

  for (let i = 0; i < bagCanContain.length; i++) {
    sum =
      sum +
      bag[bagCanContain[i]] +
      bag[bagCanContain[i]] * parseSumRule(rules, bagCanContain[i]);
  }

  return sum;
}

export function part1(rules: Rules, key: string, amount: number) {
  const allBagNames = Object.getOwnPropertyNames(rules);
  return allBagNames.filter((name) => parseRule(rules, name, key, amount))
    .length;
}

export function part2(rules: Rules, key: string) {
  return parseSumRule(rules, key);
}
