export type InputType = {
  door: number;
  card: number;
};

function transfromSubjectNumber(value: number, subjectNumber: number) {
  return (value * subjectNumber) % 20201227;
}

export function part1({ card, door }: InputType): number {
  let value = 1;
  let subjectNumber = 7;
  let cardLoopSize: number;
  let doorLoopSize: number;

  let i = 1;
  while (!doorLoopSize || !cardLoopSize) {
    value = transfromSubjectNumber(value, subjectNumber);

    if (value === card) {
      cardLoopSize = i;
    }

    if (value === door) {
      doorLoopSize = i;
    }

    i++;
  }

  let k = 1;
  let newValue = 1;
  let newSubjectNumber = card;
  while (k <= doorLoopSize) {
    newValue = transfromSubjectNumber(newValue, newSubjectNumber);
    k++;
  }

  return newValue;
}

export function part2(input: InputType) {}
