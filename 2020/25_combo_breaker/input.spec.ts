import { InputType, part1, part2 } from "./src";

xdescribe("25 - Combo Breaker", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 14897079", async () => {
        const input: InputType = {
          door: 17807724,
          card: 5764801,
        };
        expect(part1(input)).toEqual(14897079);
      });
    });

    describe("Input file", () => {
      it("Should return 16311885", async () => {
        const input: InputType = {
          card: 6270530,
          door: 14540258,
        };
        expect(part1(input)).toEqual(16311885);
      });
    });
  });

  // describe("Part 2", () => {
  //   describe("Test file", () => {
  //     it("Should return 62", async () => {
  //       const input = await parseInput(testFile);
  //       expect(part2(input)).toEqual(62);
  //     });
  //   });

  //   describe("Input file", () => {
  //     it("Should return 67587168", async () => {
  //       const input = await parseInput(inputFile);
  //       expect(part2(input)).toEqual(67587168);
  //     });
  //   });
  // });
});
