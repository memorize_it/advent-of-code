import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/18_operation_order/input.txt";

xdescribe("18 - Operation Order", () => {
  describe("Part 1", () => {
    const testCases: [string, number][] = [
      ["1 + 2 * 3 + 4 * 5 + 6", 71],
      ["1 + (2 * 3)", 7],
      ["(2 * (3 + 1))", 8],
      ["1 + (2 * (3 + 1))", 9],
      ["1 + (2 * 3) + (4 * (5 + 6))", 51],
      ["2 * 3 + (4 * 5)", 26],
      ["5 + (8 * 3 + 9 + 3 * 4 * 3)", 437],
      ["5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 12240],
      ["((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 13632],
    ];
    describe("Test file", () => {
      testCases.forEach(([t, r]) => {
        describe(`When "${t}"`, () => {
          it(`Should return ${r}`, async () => {
            expect(part1([t])).toEqual(r);
          });
        });
      });
    });

    describe("Input file", () => {
      it("Should return 45283905029161", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(45283905029161);
      });
    });
  });

  describe("Part 2", () => {
    const testCases: [string, number][] = [
      ["1 + 2 * 3 + 4 * 5 + 6", 231],
      ["1 + (2 * 3)", 7],
      ["7 + (4 * (5 + 6))", 51],
      ["1 + (2 * 3) + (4 * (5 + 6))", 51],
      ["2 * 3 + (4 * 5)", 46],
      ["5 + (8 * 3 + 9 + 3 * 4 * 3)", 1445],
      ["5 * 9 * (7 * 3 * 3 + 9 * 3 + (8 + 6 * 4))", 669060],
      ["((2 + 4 * 9) * (6 + 9 * 8 + 6) + 6) + 2 + 4 * 2", 23340],
      ["4 * 9 + (6 + 4) * 6", 456],
      ["6 * (8 * (4 * 4)) + (2 * 5)", 828],
    ];

    describe("Test file", () => {
      testCases.forEach(([t, r]) => {
        describe(`When "${t}"`, () => {
          it(`Should return ${r}`, async () => {
            expect(part2([t])).toEqual(r);
          });
        });
      });
    });

    describe("Input file", () => {
      it("Should return 216975281211165", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(216975281211165);
      });
    });
  });
});
