import { parseInputAsArray } from "../../utils/input";

type InputType = string[];

interface Expression {
  symbol?: string;
  expression: string[];
}

interface EvaluatedExpression {
  symbol?: string;
  value: number;
}

export async function parseInput(path: string): Promise<InputType> {
  return parseInputAsArray(path);
}

function evaluateExpression(expression: string[]): number {
  if (!expression.length) {
    throw new Error("Expression is empty");
  }

  const n = Number.parseInt(expression[0]);

  if (expression.length < 2) {
    return n;
  }

  let currentExpression: number;
  let nextExpression: number;
  let nextSimbol: string;

  if (!Number.isInteger(n)) {
    if (expression.length < 2) {
      throw new Error("Expression is too short");
    }
    let level = 1;
    let lastParenthesisIndex: number;
    for (
      let i = 1;
      lastParenthesisIndex === undefined && i < expression.length;
      i++
    ) {
      const current = expression[i];

      if (current === ")" || current === "(") {
        if (current === ")") {
          level++;
        } else if (current === "(") {
          level--;
        }

        if (level === 0) {
          lastParenthesisIndex = i;
        }
      }
    }

    if (lastParenthesisIndex === undefined) {
      throw new Error("Unable to find next parenthesis");
    }

    currentExpression = evaluateExpression(
      expression.slice(1, lastParenthesisIndex)
    );

    const remaining = expression.slice(
      lastParenthesisIndex + 2,
      expression.length
    );

    if (!remaining.length) {
      return currentExpression;
    }

    nextSimbol = expression[lastParenthesisIndex + 1];
    nextExpression = evaluateExpression(remaining);
  } else {
    currentExpression = n;
    nextSimbol = expression[1];
    nextExpression = evaluateExpression(expression.slice(2, expression.length));
  }

  switch (nextSimbol) {
    case "+":
      return currentExpression + nextExpression;
    case "*":
      return currentExpression * nextExpression;
  }
}

function sanitizeInput(i: string): string[] {
  return i
    .replace(/\(/gi, " ( ")
    .replace(/\)/gi, " ) ")
    .split(" ")
    .filter(Boolean)
    .reverse();
}

export function part1(input: InputType) {
  return input
    .map((i) => evaluateExpression(sanitizeInput(i)))
    .reduce((c, n) => c + n, 0);
}

function evaluateExpression2(inputExpression: string[]): EvaluatedExpression {
  let decomposedExpression = split(inputExpression, "*");

  if (decomposedExpression.length === 1) {
    decomposedExpression = split(inputExpression, "+");
  }

  const values = decomposedExpression.map(
    ({ expression, symbol }): EvaluatedExpression => {
      expression = removeUnnecessaryParenthesis(expression);

      if (expression.length === 1) {
        return {
          value: Number.parseInt(expression[0]),
          symbol,
        };
      }

      if (!expression.includes("*") || !expression.includes("+")) {
        return resolveSimpleExpression({ expression, symbol });
      }

      return {
        value: evaluateExpression2(expression).value,
        symbol,
      };
    }
  );

  let result = values[0].value;

  for (let i = 1; i < values.length; i++) {
    const { value } = values[i];
    const { symbol } = values[i - 1];
    result = symbol === "+" ? value + result : value * result;
  }

  return {
    value: result,
  };
}

function resolveSimpleExpression({
  expression,
  symbol,
}: Expression): EvaluatedExpression {
  const isAddition = !expression.includes("*");
  const filteredExpression = [...expression]
    .filter((c) => ![")", "(", "+", "*"].includes(c))
    .map((n) => Number.parseInt(n));

  const value = isAddition
    ? filteredExpression.reduce((c, v) => c + v, 0)
    : filteredExpression.reduce((c, v) => c * v, 1);

  return {
    value,
    symbol,
  };
}

function removeUnnecessaryParenthesis(expression: string[]): string[] {
  const expressionMaxIndex = expression.length - 1;

  if (expression[0] !== ")" || expression[expressionMaxIndex] !== "(") {
    return expression;
  }

  for (let i = 1, level = 1; i < expression.length; i++) {
    const current = expression[i];

    if (current === ")") {
      level = level + 1;
    } else if (current === "(") {
      level = level - 1;
    }

    if (level === 0) {
      if (i == expressionMaxIndex) {
        return expression.slice(1, expressionMaxIndex);
      }

      if (!expression.includes("*") || !expression.includes("+")) {
        return [...expression].filter((c) => ![")", "("].includes(c));
      }

      let decomposedExpression = split(expression, "*");

      if (decomposedExpression.length === 1) {
        decomposedExpression = split(expression, "+");
      }

      if (decomposedExpression.length === 1) {
        throw new Error("Missing case");
      }

      return decomposedExpression
        .map(({ expression, symbol }) => ({
          value: evaluateExpression2(expression).value,
          symbol,
        }))
        .reduce((c, { value, symbol }) => [...c, value.toString(), symbol], [])
        .filter(Boolean);
    }
  }
}

function split(inputExpression: string[], separator: string) {
  const decomposedExpression: Expression[] = [];

  for (
    let i = 0, level = 0, previousIndex = 0;
    i < inputExpression.length;
    i++
  ) {
    const current = inputExpression[i];

    if (current === ")") {
      level++;
    } else if (current === "(") {
      level--;
    }

    if (current === separator && level === 0) {
      decomposedExpression.push({
        expression: [...inputExpression].slice(previousIndex, i),
        symbol: current,
      });
      previousIndex = i + 1;
    } else if (i === inputExpression.length - 1) {
      decomposedExpression.push({
        expression: [...inputExpression].slice(previousIndex, i + 1),
      });
    }
  }

  return decomposedExpression;
}

export function part2(input: InputType) {
  return input
    .map((line) => sanitizeInput(line))
    .map((sanitizedInput, i) => {
      const { value } = evaluateExpression2(sanitizedInput);
      return value;
    })
    .reduce((c, n) => c + n, 0);
}
