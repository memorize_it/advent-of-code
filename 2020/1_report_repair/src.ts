export function parseExpenseReport2(input: number[]): number {
  for (let i = 0; i < input.length - 1; i++) {
    for (let k = i + 1; k < input.length; k++) {
      if (input[i] + input[k] === 2020) {
        return input[i] * input[k];
      }
    }
  }

  throw new Error("Not matching results");
}

export function parseExpenseReport3(input: number[]): number {
  for (let i = 0; i < input.length - 2; i++) {
    for (let j = i + 1; j < input.length - 1; j++) {
      for (let k = j + 1; k < input.length; k++) {
        if (input[i] + input[j] + input[k] === 2020) {
          return input[i] * input[j] * input[k];
        }
      }
    }
  }

  throw new Error("Not matching results");
}

export function parseExpenseReport(
  input: number[],
  recursion: number,
  target: number,
  solutionMembers: number[]
): number {
  if (recursion === 1) {
    if (input.includes(target)) {
      solutionMembers.push(target);
      return solutionMembers.reduce((p, c) => p * c, 1);
    }

    throw new Error("Not matching results");
  }

  for (let i = 0; i < input.length + 1 - recursion; i++) {
    const current = input[i];
    if (current < target) {
      let newInput = [...input];
      newInput.shift();

      try {
        return parseExpenseReport(newInput, recursion - 1, target - current, [
          ...solutionMembers,
          current,
        ]);
      } catch {}
    }
  }

  throw new Error("Not matching results");
}

export function parseExpenseReportRecursive(
  input: number[],
  recursion: number
): number {
  return parseExpenseReport(input, recursion, 2020, []);
}
