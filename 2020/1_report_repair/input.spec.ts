import { parseExpenseReport2, parseExpenseReport3, parseExpenseReportRecursive } from "./src";
import { test, part1and2 } from "./input";

xdescribe("1 - Report Repair", () => {
  describe("Part 1", () => {
    it("Should return 514579", () => {
      expect(parseExpenseReport2(test)).toEqual(514579);
    });

    it("Should return 538464", () => {
      expect(parseExpenseReport2(part1and2)).toEqual(538464);
    });
  });

  describe("Part 2", () => {
    it("Should return 241861950", () => {
      expect(parseExpenseReport3(test)).toEqual(241861950);
    });

    it("Should return 278783190", () => {
      expect(parseExpenseReport3(part1and2)).toEqual(278783190);
    });
  });

  describe("Part 3", () => {
    it("Should return 514579", () => {
      expect(parseExpenseReportRecursive(test, 2)).toEqual(514579);
    });

    it("Should return 538464", () => {
      expect(parseExpenseReportRecursive(part1and2, 2)).toEqual(538464);
    });

    it("Should return 241861950", () => {
      expect(parseExpenseReportRecursive(test, 3)).toEqual(241861950);
    });

    it("Should return 278783190", () => {
      expect(parseExpenseReportRecursive(part1and2, 3)).toEqual(278783190);
    });
  });
});
