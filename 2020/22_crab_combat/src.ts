import { parseInputAsArray } from "../../utils/input";

type InputType = number[][];

export async function parseInput(path: string): Promise<InputType> {
  const input = await parseInputAsArray(path);

  let result: InputType = [];
  let currentArray: number[] = [];
  input.forEach((l) => {
    const n = Number.parseInt(l);
    if (Number.isNaN(n)) {
      if (currentArray.length) {
        result.push(currentArray);
        currentArray = [];
      }
    } else {
      currentArray.push(n);
    }
  });

  result.push(currentArray);

  return result;
}

export function part1([player1Deck, player2Deck]: InputType) {
  let winner: number[];
  while (!winner) {
    ({ player1Deck, player2Deck } = parseTurnNonRecursive({
      player1Deck,
      player2Deck,
    }));
    if (player1Deck.length === 0) {
      winner = player2Deck;
    } else if (player2Deck.length === 0) {
      winner = player1Deck;
    }
  }

  return calculateWinnerScore(winner);
}

function calculateWinnerScore(winner: number[]) {
  return winner.reduce((c, v, i) => c + v * (winner.length - i), 0);
}

function parseTurnNonRecursive({
  player1Deck,
  player2Deck,
}: {
  player1Deck: number[];
  player2Deck: number[];
}) {
  const player1Plays = player1Deck.shift();
  const player2Plays = player2Deck.shift();

  if (player1Plays > player2Plays) {
    player1Deck.push(player1Plays);
    player1Deck.push(player2Plays);
  } else {
    player2Deck.push(player2Plays);
    player2Deck.push(player1Plays);
  }

  return { player1Deck, player2Deck };
}

export function part2([player1Deck, player2Deck]: InputType) {
  let winner: number[];
  let player1History: Set<string> = new Set();
  let player2History: Set<string> = new Set();

  let round = 1;
  while (!winner) {
    ({ player1Deck, player2Deck } = parseGameTurn({
      player1Deck,
      player2Deck,
      player1History,
      player2History,
      game: 1,
      round,
    }));

    if (player1Deck.length === 0) {
      winner = player2Deck;
    } else if (player2Deck.length === 0) {
      winner = player1Deck;
    }
    round++;
  }

  return calculateWinnerScore(winner);
}

function parseGameTurn({
  player1Deck,
  player2Deck,
  player1History,
  player2History,
  round,
  game,
}: {
  player1Deck: number[];
  player2Deck: number[];
  player1History: Set<string>;
  player2History: Set<string>;
  round: number;
  game: number;
}) {
  const player1DeckStatus = player1Deck.join(",");
  const player2DeckStatus = player2Deck.join(",");

  if (
    player1History.has(player1DeckStatus) &&
    player2History.has(player2DeckStatus)
  ) {
    return {
      player1Deck,
      player2Deck: [],
    };
  }

  player1History.add(player1DeckStatus);
  player2History.add(player2DeckStatus);

  const player1Plays = player1Deck.shift();
  const player2Plays = player2Deck.shift();

  if (
    player1Deck.length >= player1Plays &&
    player2Deck.length >= player2Plays
  ) {
    let player1Sub = player1Deck.slice(0, player1Plays);
    let player2Sub = player2Deck.slice(0, player2Plays);

    let winner = false;
    let round = 1;

    let player1SubHistory: Set<string> = new Set();
    let player2SubHistory: Set<string> = new Set();

    while (!winner) {
      ({ player1Deck: player1Sub, player2Deck: player2Sub } = parseGameTurn({
        player1Deck: player1Sub,
        player2Deck: player2Sub,
        player1History: player1SubHistory,
        player2History: player2SubHistory,
        game: game + 1,
        round,
      }));

      if (player2Sub.length === 0) {
        winner = true;
        player1Deck.push(player1Plays);
        player1Deck.push(player2Plays);
      } else if (player1Sub.length === 0) {
        winner = true;
        player2Deck.push(player2Plays);
        player2Deck.push(player1Plays);
      }
      round++;
    }
  } else {
    if (player1Plays >= player2Plays) {
      player1Deck.push(player1Plays);
      player1Deck.push(player2Plays);
    } else {
      player2Deck.push(player2Plays);
      player2Deck.push(player1Plays);
    }
  }

  return {
    player1Deck,
    player2Deck,
  };
}
