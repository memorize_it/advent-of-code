import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/22_crab_combat/input.txt";
const testFile = "./2020/22_crab_combat/test.input.txt";

xdescribe("22 - Crab Combat", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 306", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(306);
      });
    });

    describe("Input file", () => {
      it("Should return 32083", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(32083);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 291", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(291);
      });
    });

    describe("Input file", () => {
      it("Should return 35495", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(35495);
      });
    });
  });
});
