import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

type Ops = {
  op: "nop" | "acc" | "jmp";
  sign: "+" | "-";
  num: number;
};

export async function parseInput(path: string): Promise<Ops[]> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: Ops[] = [];
      readInterface.on("line", (line) => {
        const [op, signedNumber] = line.split(" ");

        if (op != "nop" && op != "acc" && op != "jmp") {
          return;
        }

        const sign = signedNumber[0];
        if (sign != "+" && sign != "-") {
          return;
        }

        result.push({
          op,
          sign,
          num: Number.parseInt(signedNumber.slice(1)),
        });
      });

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

export function part1(ops: Ops[]) {
  let count = 0;
  const visitedAdr = [];
  for (let i = 0; i < ops.length; ) {
    if (visitedAdr.includes(i)) {
      return count;
    }

    visitedAdr.push(i);

    const { op, sign, num } = ops[i];
    switch (op) {
      case "acc":
        count = sign === "+" ? count + num : count - num;
        i = i + 1;
        break;
      case "jmp":
        i = sign === "+" ? i + num : i - num;
        break;
      case "nop":
        i = i + 1;
        break;
    }
  }

  throw new Error("Should not finish");
}

export function part2(ops: Ops[]) {
  function switchNextInstruction() {
    operations = ops.map((o) => ({ ...o }));
    let operation = operations[jmpOrNopIndexes[updatedIndex]];
    operation.op = operation.op === "jmp" ? "nop" : "jmp";
    updatedIndex = updatedIndex + 1;
    count = 0;
    visitedAdr = [];
  }

  let jmpOrNopIndexes = ops
    .map(({ op }) => op)
    .reduce((c, o, i) => {
      if (o === "acc") {
        return c;
      }
      c.push(i);
      return c;
    }, []);

  let updatedIndex = 0;
  let operations = [];
  let count = 0;
  let visitedAdr = [];
  switchNextInstruction();

  for (let i = 0; i < operations.length; ) {
    if (visitedAdr.includes(i)) {
      i = 0;

      switchNextInstruction();

      count = 0;
      visitedAdr = [];
      continue;
    }

    visitedAdr.push(i);

    const { op, sign, num } = operations[i];
    switch (op) {
      case "acc":
        count = sign === "+" ? count + num : count - num;
        i = i + 1;
        break;
      case "jmp":
        i = sign === "+" ? i + num : i - num;
        break;
      case "nop":
        i = i + 1;
        break;
    }
  }

  return count;
}
