import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/8_handheld_halting/input.txt";
const testFile = "./2020/8_handheld_halting/test.input.txt";

xdescribe("Day 8 : Handheld Halting", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 5", async () => {
        const input = await parseInput(testFile);
        expect(part1(input)).toEqual(5);
      });
    });

    describe("Input file", () => {
      it("Should return 1317", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input)).toEqual(1317);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 8", async () => {
        const input = await parseInput(testFile);
        expect(part2(input)).toEqual(8);
      });
    });

    describe("Input file", () => {
      it("Should return 1033", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input)).toEqual(1033);
      });
    });
  });
});
