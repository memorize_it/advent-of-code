import { parseInput, part1, part2 } from "./src";

const inputFile = "./2020/9_encoding_error/input.txt";
const testFile = "./2020/9_encoding_error/test.input.txt";

xdescribe("9 - Encoding Error", () => {
  describe("Part 1", () => {
    describe("Test file", () => {
      it("Should return 127", async () => {
        const input = await parseInput(testFile);
        expect(part1(input, 5, 5)).toEqual(127);
      });
    });

    describe("Input file", () => {
      it("Should return 400480901", async () => {
        const input = await parseInput(inputFile);
        expect(part1(input, 25, 25)).toEqual(400480901);
      });
    });
  });

  describe("Part 2", () => {
    describe("Test file", () => {
      it("Should return 62", async () => {
        const input = await parseInput(testFile);
        expect(part2(input, 5, 5)).toEqual(62);
      });
    });

    describe("Input file", () => {
      it("Should return 67587168", async () => {
        const input = await parseInput(inputFile);
        expect(part2(input, 25, 25)).toEqual(67587168);
      });
    });
  });
});
