import { createReadStream } from "fs";
import { resolve } from "path";
import { createInterface } from "readline";

export async function parseInput(path: string): Promise<number[]> {
  return new Promise((res, rej) => {
    try {
      const readInterface = createInterface({
        input: createReadStream(resolve(path)),
      });

      const result: number[] = [];
      readInterface.on("line", (line) => result.push(Number.parseInt(line)));

      readInterface.on("close", () => res(result));
    } catch (error) {
      rej(error);
    }
  });
}

export function part1(
  numbers: number[],
  preamble: number,
  previous: number
): number {
  mainLoop: for (let i = preamble; i < numbers.length; i++) {
    const currentNumber = numbers[i];
    const possibleNumbers = [...numbers].slice(i - previous, i);
    for (let k = 0; k < possibleNumbers.length - 1; k++) {
      const first = possibleNumbers[k];
      for (let j = 1; j < possibleNumbers.length; j++) {
        const second = possibleNumbers[j];
        if (first + second === currentNumber) {
          continue mainLoop;
        }
      }
    }

    return currentNumber;
  }

  throw new Error("Not number found");
}

function findAddition(numbers: number[], target: number): number[] {
  for (let startIndex = 0; startIndex < numbers.length; startIndex++) {
    const current = numbers[startIndex];
    let currentNumbers = [current];
    let currentTarget = target;

    for (
      let numbersToAdd = 1;
      numbersToAdd < numbers.length - startIndex;
      numbersToAdd++
    ) {
      const allNumbersToAdd = [...numbers].slice(
        startIndex,
        startIndex + numbersToAdd
      );
      const numberToAdd = allNumbersToAdd.reduce((c, n) => c + n, 0);
      if (numberToAdd === target) {
        return allNumbersToAdd;
      }
    }
  }

  throw new Error("Not found");
}

export function part2(
  numbers: number[],
  preamble: number,
  previous: number
): number {
  const vuln = part1(numbers, preamble, previous);

  const sortedNumbers = numbers.filter((n) => n !== vuln);
  const allNumbersSum = findAddition(sortedNumbers, vuln).sort((a, b) => a - b);

  return allNumbersSum[0] + allNumbersSum[allNumbersSum.length - 1];
}
