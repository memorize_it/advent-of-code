import { parseInputAsString } from "../../utils/deno/input.ts";

type InputType = string[];

export const parseInput = parseInputAsString;

export function part1(input: InputType) {
  return input[0];
}

export function part2(input: InputType) {
  return input[0];
}
