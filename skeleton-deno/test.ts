import { assertEquals } from "https://deno.land/std/testing/asserts.ts";
import {
  describe,
  it,
  xdescribe,
  xit,
} from "https://deno.land/x/test_suite/mod.ts";
import { parseInput, part1, part2 } from "./src.ts";

const part1TestResult = 0;
const part1Result = 0;
const part2TestResult = 0;
const part2Result = 0;

describe("$DAY - $TITLE", () => {
  describe("Part 1", () => {
    it("Test file", async () => {
      assertEquals(
        part1(await parseInput("./test.input.txt")),
        part1TestResult,
      );
    });

    xit("Input file", async () => {
      assertEquals(part1(await parseInput("./input.txt")), part1Result);
    });
  });

  xdescribe("Part 2", () => {
    it("Test file", async () => {
      assertEquals(
        part2(await parseInput("./test.input.txt")),
        part2TestResult,
      );
    });

    xit("Input file", async () => {
      assertEquals(part2(await parseInput("./input.txt")), part2Result);
    });
  });
});
